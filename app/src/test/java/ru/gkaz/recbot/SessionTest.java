package ru.gkaz.recbot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import ru.gkaz.recbot.bot.core.Session;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class SessionTest {
    private static final String DEVICE_ID    = "testId";
    private static final String DEVICE_ALIAS = "testAlias";
    private static final Long TIMESTAMP      = new Long(12345);

    @Mock
    JSONObject mObject;
    @Before
    public void initMocks() throws JSONException {
        mObject = mock(JSONObject.class);
        when(mObject.put(anyString(), any())).thenReturn(mObject);
    }

    @Test
    public void toJSON_isCorrect() {
        Session session = new Session(DEVICE_ID, DEVICE_ALIAS, TIMESTAMP);
        String json = "{\"device-alias\":\"testAlias\",\"device-id\":\"testId\",\"timestamp\":12345}";
        try {
            assertEquals(json, session.toJSON().toString());
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    public void getSequenceNumber_isCorrect() {
        Session session = new Session(DEVICE_ID, DEVICE_ALIAS, TIMESTAMP);
        assertEquals(0, session.getSequenceNumber());
        session.sequenceIncrement();
        assertEquals(1, session.getSequenceNumber());
    }
}