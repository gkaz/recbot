package ru.gkaz.recbot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.DropDownPreference;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import ru.gkaz.recbot.R;

public class SettingsActivity extends AppCompatActivity {
    private static Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getIntent();
        SettingsFragment fragment = new SettingsFragment(intent.getCharSequenceArrayExtra("cameraIDs"));
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, fragment)
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        private CharSequence[] mCameraIDs;
        public SettingsFragment(CharSequence[] cameraIDs) {
            mCameraIDs = cameraIDs;
        }
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            DropDownPreference cameraFrontPref = findPreference("camera_front");
            DropDownPreference cameraBackPref = findPreference("camera_back");
            cameraFrontPref.setEntries(mCameraIDs);
            cameraFrontPref.setEntryValues(mCameraIDs);
            cameraBackPref.setEntries(mCameraIDs);
            cameraBackPref.setEntryValues(mCameraIDs);

            EditTextPreference movementDetectionThreshold
                    = findPreference(getString(R.string.common_movement_detection_threshold_key));
            movementDetectionThreshold.setOnBindEditTextListener(
                    new EditTextPreference.OnBindEditTextListener() {
                        @Override
                        public void onBindEditText(@NonNull EditText editText) {
                            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        }
                    }
            );
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
        startActivity(intent);
        return true;
    }

}