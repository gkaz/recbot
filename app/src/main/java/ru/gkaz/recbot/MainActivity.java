package ru.gkaz.recbot;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.Locale;

import ru.gkaz.recbot.bot.RecBot;
import ru.gkaz.recbot.bot.RecBotListener;
import ru.gkaz.recbot.bot.core.Status;

public class MainActivity extends AppCompatActivity implements RecBotListener {
    /**
     * Application permissions.
     */
    private final static String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.CAMERA
    };

    private static final String TAG = MainActivity.class.getCanonicalName();

    private RecBot mRecBot;

    /**
     * Check if the application has all the needed permissions.
     *
     * @return true if it has, false otherwise.
     */
    private boolean hasPermissions() {
        return hasPermissions(MainActivity.this, PERMISSIONS);
    }

    /**
     * Request the needed permissions.
     */
    private void requestPermissions() {
        int PERMISSION_ALL = 1;
        ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, PERMISSION_ALL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "Permissions granted");
        init();
    }

    private void init() {
        mRecBot = new RecBot(this, this);
        mRecBot.init();
        setClearButtonState(true);
        setPingButtonState(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (! hasPermissions()) {
            requestPermissions();
        } else {
            init();
        }
        Log.i(TAG, "Created");
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRecBot != null) {
            mRecBot.destroy();
        }
        Log.i(TAG, "Stopped");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRecBot != null) {
            mRecBot.destroy();
        }
        Log.i(TAG, "Paused");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Resumed");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRecBot != null) {
            mRecBot.destroy();
        }
        Log.i(TAG, "Destroyed");
    }

    public void onButtonClear(View view) {
        mRecBot.clear();
    }

    public void captureToggle(View view) {
        if (mRecBot.getState() == RecBot.State.STARTED) {
            mRecBot.stop();
        } else if (mRecBot.getState() == RecBot.State.STOPPED) {
            mRecBot.start();
        }
    }

    public void onNotify(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onSettingsButtonClick(View view) {
        openSettings();
    }

    public void onTestButtonClick(View view) {
        mRecBot.ping();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void openSettings() {
        Log.d(TAG, "Opening settings...");
        CameraManager cameraManager = getSystemService(CameraManager.class);
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        try {
            Log.i(TAG, "Cameras: ");
            for (String id : cameraManager.getCameraIdList()) {
                Log.i(TAG, "    " + id);
            }
            intent.putExtra("cameraIDs", cameraManager.getCameraIdList());
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        MainActivity.this.startActivity(intent);
    }

    @Override
    public void onDisconnection(String message) {
        onNotify(message);
        setCaptureButtonState(false);
        setPingButtonState(false);
    }

    @Override
    public void onConnection(String message) {
        onNotify(message);
        setCaptureButtonState(true);
        setPingButtonState(true);
        setClearButtonState(true);
    }

    @Override
    public void onSessionSend() {
        setCaptureButtonState(false);
        onNotify("Saving and sending recording session...");
    }

    @Override
    public void onSessionStop() {
        onNotify("Stopping the recording session...");
        setCaptureButtonText("Start");
        setCaptureButtonState(true);
        setClearButtonState(true);
    }

    @Override
    public void onSessionStart() {
        setCaptureButtonText("Stop");
        setCaptureButtonState(true);
        setClearButtonState(false);
    }

    @Override
    public void onConfigure() {
        openSettings();
    }

    private void setCaptureButtonState(final boolean isEnabled) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button buttonRecording = (Button) findViewById(R.id.recording);
                buttonRecording.setEnabled(isEnabled);
            }
        });
    }

    private void setPingButtonState(final boolean isEnabled) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button button = findViewById(R.id.socketMsgBtn);
                button.setEnabled(isEnabled);
            }
        });
    }

    private void setClearButtonState(final boolean isEnabled) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button button = findViewById(R.id.button_clear);
                button.setEnabled(isEnabled);
            }
        });
    }

    private void setSettingsButtonState(final boolean isEnabled) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button button = findViewById(R.id.buttonOpenSettings);
                button.setEnabled(isEnabled);
            }
        });
    }

    private void setCaptureButtonText(final String text) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button buttonRecording = (Button) findViewById(R.id.recording);
                buttonRecording.setText(text);
            }
        });
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onReconfigure() {
        Log.w(TAG, "Restarting the application...");
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onReady(String deviceID, String deviceAlias) {
        TextView deviceIdView    = findViewById(R.id.device_id_view);
        TextView deviceAliasView = findViewById(R.id.device_alias_view);
        deviceIdView.setText(deviceID);
        deviceAliasView.setText(deviceAlias);
    }

    @Override
    public void onStatus(final Status status) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView statusView = findViewById(R.id.status_view);
                statusView.setText(String.format(
                        Locale.ENGLISH,
                        "uptime: %s\nsession: \n  camera: %s\n  recordings: %d\n  %s",
                        DateUtils.formatElapsedTime(status.getUptime() / 1000),
                        status.getCurrentCamera(),
                        status.getRecordings(),
                        status.getRecordingStatus() ? "recording" : "stopped"));
            }
        });
    }
}