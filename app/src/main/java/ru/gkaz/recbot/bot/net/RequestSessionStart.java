package ru.gkaz.recbot.bot.net;

import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.core.Status;

public class RequestSessionStart extends RequestSession {

    public RequestSessionStart(Session session, Status status) {
      super(RequestSession.ACTION_START, session, status);
    }
}
