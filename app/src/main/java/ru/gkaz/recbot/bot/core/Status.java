package ru.gkaz.recbot.bot.core;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Status implements Type {
    public static final String KEY                   = "status";
    public static final String DEVICE_ID_KEY         = "device-id";
    public static final String DEVICE_ALIAS_KEY      = "device-alias";
    public static final String BATTERY_LEVEL_KEY     = "battery-level";
    public static final String UPTIME_KEY            = "uptime";
    public static final String RECORDING_STATUS      = "recording?";
    public static final String CURRENT_CAMERA_KEY    = "current-camera";
    public static final String RECORDINGS_KEY        = "recordings";
    public static final String RECORDINGS_TOTAL_KEY  = "recordings-total";

    private String mDeviceID;
    private String mDeviceAlias = null;
    private int mBatteryLevel;
    private long mUptime;
    private boolean mIsRecording = false;
    private String mCurrentCamera;
    private int mRecordings = 0;
    private int mRecordingsTotal = 0;

    public Status setDeviceID(String deviceID) {
        mDeviceID = deviceID;
        return this;
    }

    public String getDeviceID() {
        return mDeviceID;
    }

    public Status setDeviceAlias(String deviceAlias) {
        mDeviceAlias = deviceAlias;
        return this;
    }

    public String getDeviceAlias() {
        return mDeviceAlias;
    }

    public Status setBatteryLevel(int level) {
        mBatteryLevel = level;
        return this;
    }

    public Status setUptime(long uptime) {
        mUptime = uptime;
        return this;
    }

    public long getUptime() {
        return mUptime;
    }

    public Status setRecordingStatus(boolean isRecording) {
        mIsRecording = isRecording;
        return this;
    }

    public boolean getRecordingStatus() {
        return mIsRecording;
    }

    public Status setCurrentCamera(String camera) {
        mCurrentCamera = camera;
        return this;
    }

    public String getCurrentCamera() {
        return mCurrentCamera;
    }

    public Status setRecordings(int number) {
        mRecordings = number;
        return this;
    }

    public Status setRecordingsTotal(int number) {
        mRecordingsTotal = number;
        return this;
    }

    public int getRecordings() {
        return mRecordings;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject status = new JSONObject();
        try {
            status.put(DEVICE_ID_KEY, mDeviceID);
            status.put(DEVICE_ALIAS_KEY, mDeviceAlias);
            status.put(BATTERY_LEVEL_KEY, mBatteryLevel);
            status.put(UPTIME_KEY, mUptime);
            status.put(RECORDING_STATUS, mIsRecording);
            status.put(CURRENT_CAMERA_KEY, mCurrentCamera);
            status.put(RECORDINGS_KEY, mRecordings);
            status.put(RECORDINGS_TOTAL_KEY, mRecordingsTotal);
        } catch (JSONException e) {
            Log.e(Status.class.getSimpleName(), e.getLocalizedMessage());
            return null;
        }
        return status;
    }

    public String toString() {
        return toJSON().toString();
    }
}
