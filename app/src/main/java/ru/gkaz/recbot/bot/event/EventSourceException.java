package ru.gkaz.recbot.bot.event;

public class EventSourceException extends Exception {
    public EventSourceException(String message) {
        super(message);
    }
}
