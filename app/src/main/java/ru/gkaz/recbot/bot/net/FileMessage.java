package ru.gkaz.recbot.bot.net;

import java.io.File;

import ru.gkaz.recbot.bot.net.core.Message;
import ru.gkaz.recbot.bot.core.Session;

/**
 * This class describes a WebSocket API message.
 */
public class FileMessage implements Message {
    /**
     * File to send.
     */
    private File mFile;
    private String mDeviceID    = null;
    private String mDeviceAlias = null;
    private Session mSession    = null;

    public FileMessage(File file, String deviceID, String deviceAlias) {
        mFile        = file;
        mDeviceID    = deviceID;
        mDeviceAlias = deviceAlias;
    }

    public FileMessage(File file, Session session) {
        this(file, session.getDeviceID(), session.getDeviceAlias());
        mSession = session;
    }

    public File getFile() {
        return mFile;
    }

    public String getDeviceID() {
        return mDeviceID;
    }

    public String getDeviceAlias() {
        return mDeviceAlias;
    }

    public Session getSession() {
        return mSession;
    }

    public byte[] getContent() {
        return mFile.toString().getBytes();
    }
}
