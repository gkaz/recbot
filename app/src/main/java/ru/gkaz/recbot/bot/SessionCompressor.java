package ru.gkaz.recbot.bot;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ru.gkaz.recbot.bot.core.Session;

public class SessionCompressor {
    public static final String TAG = SessionCompressor.class.getSimpleName();
    public static final int BUFFER_SIZE = 256;
    private File mRootDirectory;
    public SessionCompressor(File rootDirectory) {
        mRootDirectory = rootDirectory;
    }

    public void zip(List<String> files, File zipFile, Session session) throws IOException {
        BufferedInputStream origin = null;
        ZipOutputStream out = new ZipOutputStream(
                new BufferedOutputStream(new FileOutputStream(zipFile)));
        out.setLevel(0);
        try {
            byte data[] = new byte[BUFFER_SIZE];

            for (String fileName : files) {
                FileInputStream fi = new FileInputStream(mRootDirectory+ "/" + fileName);
                origin = new BufferedInputStream(fi, BUFFER_SIZE);
                try {
                    ZipEntry entry = new ZipEntry(fileName);
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                        out.write(data, 0, count);
                    }
                }
                finally {
                    origin.close();
                }
            }
        }
        finally {
            out.close();
        }
    }

    public File compress(Session session, boolean deleteOriginals) throws IOException {
        File zipFile = new File(
                mRootDirectory + "/"
                        + session.getDeviceID() + "_" + session.getTimestamp() + ".zip");
        File[] files = mRootDirectory.listFiles();
        List<String> sessionFiles = new ArrayList<>();
        String deviceID = session.getDeviceID();
        String timestamp = Long.toString(session.getTimestamp());
        for (File f : files) {
            String fileName = f.getName();
            if (fileName.contains(deviceID) && fileName.contains(timestamp)) {
                sessionFiles.add(fileName);
            }
        }

        zip(sessionFiles, zipFile, session);

        if (deleteOriginals) {
            for (File f : files) {
                f.delete();
            }
        }

        return zipFile;
    }
}
