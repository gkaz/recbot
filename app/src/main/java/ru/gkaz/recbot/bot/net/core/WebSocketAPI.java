package ru.gkaz.recbot.bot.net.core;

import java.util.List;
import java.util.Map;

import ru.gkaz.recbot.bot.net.Request;

public interface WebSocketAPI {
    /**
     * Handle a command received through the socket.
     * @param request Incoming request.
     * @return Response to the client.
     */
    Message handleRequest(Request request);

    /**
     * Handle WebSocket errors.
     *
     * @param errorMessage
     */
    void handleError(final String errorMessage);

    /**
     * Handle WebSocket connection.
     *
     * @param headers
     */
    Message handleConnection(Map<String, List<String>> headers);
}
