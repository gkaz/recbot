package ru.gkaz.recbot.bot.net;

public class RequestPing extends Request {
    public static final String COMMAND = "ping";
    public RequestPing(String deviceID) {
        super(deviceID, COMMAND);
    }
}
