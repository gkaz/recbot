package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;

import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.core.Status;

public abstract class RequestSession extends Request {
    public static final String ACTION_START = "session-start";
    public static final String ACTION_STOP  = "session-stop";
    public static final String ACTION_SEND  = "session-send";

    public RequestSession(String command, Session session, Status status) {
        super(session.getDeviceID(), command);
        try {
            put(Session.KEY, session.toJSON());
            put(Status.KEY,  status.toJSON());
        } catch (JSONException e) {
            Log.e(RequestSession.class.getSimpleName(), e.getMessage());
        }
    }
}
