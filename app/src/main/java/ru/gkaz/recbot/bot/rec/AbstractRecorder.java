package ru.gkaz.recbot.bot.rec;

import java.io.File;

/**
 * This class describes an abstract video recorder.
 */
public abstract class AbstractRecorder {
    protected boolean isRecording = false;
    protected RecorderConfig mConfig;
    protected File mOutput;

    public static class RecorderException extends Exception {
        public RecorderException(String message) {
            super(message);
        }
    }

    public AbstractRecorder(RecorderConfig config) throws RecorderException {
        mConfig = config;
    }

    /**
     * Get the recorder name.
     * @return the recorder name as a string.
     */
    public String getName() {
        return mConfig.getName();
    }

    /**
     * Check if the recorder is recording video.
     * @return true if the recorder is recording, false otherwise.
     */
    public boolean isRecording() {
        return isRecording;
    }

    /**
     * Start the recording session.
     * @param output File to store the recording to.
     * @throws RecorderException
     */
    public void start(File output) throws RecorderException {
        this.mOutput = output;
        isRecording = true;
    }

    /**
     * Get Recorder camera ID.
     * @return camera ID.
     */
    public String getCameraId() {
        return mConfig.getCameraId();
    }

    /**
     * Stop the recording session.
     * @throws RecorderException
     */
    public void stop() throws RecorderException {
        isRecording = false;
    }

    /**
     * Close the recorder and free the resources.
     * @throws RecorderException
     */
    public void close() throws RecorderException {
        stop();
    }
}
