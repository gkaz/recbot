package ru.gkaz.recbot.bot.rec;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static androidx.core.content.ContextCompat.getSystemService;

/**
 * This class allows to record image/video data from the specified camera.
 */
public class Recorder extends AbstractRecorder {
    private static final String TAG = Recorder.class.getSimpleName();

    private final CameraManager mCameraManager;
    private final Object mLock;

    private Context mContext;
    private CameraDevice mCameraDevice = null;
    private boolean mIsRecording = false;
    private CameraCaptureSession mCameraCaptureSession;
    private HandlerThread mHandlerThread;
    private Handler mBackgroundHandler;

    private MediaCodec mVideoEncoder;
    private Surface mInputSurface;
    private MediaMuxer mMediaMuxer;
    private final Handler mDrainHandler = new Handler(Looper.getMainLooper());
    private boolean mIsMuxerStarted = false;
    private Runnable mDrainEncoderRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                drainEncoder();
            } catch (RecorderException e) {
                Log.e(TAG, "Encoder runnable error", e);
            }
        }
    };

    private MediaCodec.BufferInfo mVideoBufferInfo;
    private int mTrackIndex = -1;

    private CameraDevice.StateCallback cameraCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            Log.i(TAG, "Camera " + mConfig.getCameraId() + " opened.");
            Recorder.this.mCameraDevice = cameraDevice;
            synchronized (mLock) {
                mLock.notify();
            }
        }

        @Override
        public void onClosed(@NonNull CameraDevice camera) {
            super.onClosed(camera);
            Log.i(TAG, "Camera " + mConfig.getCameraId() + " is closed");
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            Log.i(TAG, "Camera " + mConfig.getCameraId() + " disconnected.");
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            Log.e(TAG, "Camera " + cameraDevice.getId() + " error: " + error);
        }
    };

    public Recorder(Context context,
                    RecorderConfig config) throws AbstractRecorder.RecorderException {
        super(config);
        mCameraManager = getSystemService(context, CameraManager.class);
        mContext = context;
        mHandlerThread = new HandlerThread("RecorderThread");
        mHandlerThread.start();
        mBackgroundHandler = new Handler(mHandlerThread.getLooper());
        mLock = new Object();
    }

    /**
     * Prepare the recorder for recording.
     */

    @SuppressLint("MissingPermission")
    private void openCamera() throws AbstractRecorder.RecorderException {
        Log.i(TAG, "Opening camera " + mConfig.getCameraId() + "...");

        try {
            mCameraManager.openCamera(mConfig.getCameraId(), cameraCallback, mBackgroundHandler);

            Log.d(TAG, "Waiting for camera...");
            synchronized (mLock) {
                mLock.wait();
            }
        } catch (InterruptedException | CameraAccessException e) {
            Log.e(TAG, "openCamera: Camera error", e);
            throw new AbstractRecorder.RecorderException(e.getMessage());
        }

        if (mCameraDevice == null) {
            Log.e(TAG, "openCamera: Camera device is null!");
            throw new RecorderException("Could not open a camera: " + mConfig.getCameraId());
        }

        Log.i(TAG, "Preparing... done");
    }

    /**
     * Get the recorder camera.
     *
     * @return CameraDevice instance.
     */
    public CameraDevice getCameraDevice() {
        return mCameraDevice;
    }

    /**
     * Start the recording.  Save the recording to the specified file.
     *
     * @param output An output file.
     * @throws RecorderException on errors.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void start(File output) throws RecorderException {
        super.start(output);
        openCamera();
        if (!mIsRecording) {
            Log.i(TAG, "Starting...");

            mVideoBufferInfo = new MediaCodec.BufferInfo();
            Log.i(TAG, "Configuring media codec ...");
            mVideoEncoder = configureMediaCodec();
            Log.i(TAG, "Creating input surface...");
            mInputSurface = mVideoEncoder.createInputSurface();
            Log.i(TAG, "Starting media encoder...");
            mVideoEncoder.start();

            Log.i(TAG, "Creating media muxer...");
            try {
                mMediaMuxer = new MediaMuxer(output.getAbsolutePath(),
                        MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            } catch (IOException e) {
                Log.e(TAG, "start: Muxer error", e);
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "Making capture request builder...");
                final CaptureRequest.Builder builder
                        = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                setUpCaptureRequestBuilder(builder);
                List<Surface> surfaces = new ArrayList<>();
                surfaces.add(mInputSurface);
                builder.addTarget(mInputSurface);

                Log.d(TAG, "Starting a capture session...");
                mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                    @Override
                    public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                        Recorder.this.mCameraCaptureSession = cameraCaptureSession;
                        try {
                            Log.i(TAG, "Setting a repeating request...");
                            cameraCaptureSession.setRepeatingRequest(builder.build(),
                                    null,
                                    mBackgroundHandler);
                        } catch (CameraAccessException e) {
                            Log.e(TAG, e.getMessage());
                        }
                        try {
                            drainEncoder();
                        } catch (RecorderException e) {
                            Log.e(TAG, e.getLocalizedMessage());
                            try {
                                stop();
                            } catch (RecorderException re) {
                                Log.e(TAG, re.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onSurfacePrepared(@NonNull CameraCaptureSession session, @NonNull Surface surface) {
                        super.onSurfacePrepared(session, surface);
                        Log.d(TAG, "onSurfacePrepared: **********************");
                    }

                    @Override
                    public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                    }
                }, mBackgroundHandler);
            } catch (CameraAccessException | IllegalStateException e) {
                Log.w(TAG, e.getMessage());
                e.printStackTrace();
                throw new RecorderException(e.getMessage());
            }
            Log.i(TAG, "Starting... done");
            mIsRecording = true;
        }
    }

    private void drainEncoder() throws RecorderException {
        mDrainHandler.removeCallbacks(mDrainEncoderRunnable);
        while (true) {
            int bufferIndex = 0;
            try {
                bufferIndex = mVideoEncoder.dequeueOutputBuffer(mVideoBufferInfo, 0);
            } catch (IllegalStateException e) {
                Log.e(Recorder.class.getCanonicalName(), "Encoder error", e);
                stop();
                return;
            }

            if (bufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // nothing available yet
                break;
            } else if (bufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                Log.d(TAG, "bufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED");
                // should happen before receiving buffers, and should only happen once
                if (mTrackIndex >= 0) {
                    throw new RuntimeException("format changed twice");
                }
                mTrackIndex = mMediaMuxer.addTrack(mVideoEncoder.getOutputFormat());
                Log.d(TAG, "mTrackIndex: " + mTrackIndex);
                if ((!mIsMuxerStarted) && (mTrackIndex >= 0)) {
                    Log.d(TAG, "(! isMuxerStarted) && (mTrackIndex >= 0)");
                    mMediaMuxer.start();
                    mIsMuxerStarted = true;
                }
            } else if (bufferIndex < 0) {
                Log.w(TAG, "drainEncoder: bufferIndex: " + bufferIndex);
                // not sure what's going on, ignore it
            } else {
                ByteBuffer encodedData = mVideoEncoder.getOutputBuffer(bufferIndex);
                if (encodedData == null) {
                    throw new RuntimeException("couldn't fetch buffer at index " + bufferIndex);
                }

                if ((mVideoBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    mVideoBufferInfo.size = 0;
                }

                if (mVideoBufferInfo.size != 0) {
                    if (mIsMuxerStarted) {
                        encodedData.position(mVideoBufferInfo.offset);
                        encodedData.limit(mVideoBufferInfo.offset + mVideoBufferInfo.size);
                        try {
                            mMediaMuxer.writeSampleData(mTrackIndex, encodedData, mVideoBufferInfo);
                        } catch (IllegalStateException e) {
                            Log.e(TAG, e.getLocalizedMessage());
                            stop();
                        }
                    }

                }

                try {
                    mVideoEncoder.releaseOutputBuffer(bufferIndex, false);
                } catch (IllegalStateException e) {
                    Log.e(TAG, "Could not release an output buffer", e);
                    stop();
                    return;
                }

                if ((mVideoBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    break;
                }
            }
        }
        mDrainHandler.postDelayed(mDrainEncoderRunnable, 10);
    }

    /**
     * Returns the first codec capable of encoding the specified MIME type, or null if no
     * match was found.
     */
    @Nullable
    private static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        Log.d(Recorder.class.getCanonicalName(), "Codecs: ");
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            Log.i(Recorder.class.getCanonicalName(), "  " + codecInfo.getName());
        }
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            if (!codecInfo.isEncoder()) {
                continue;
            }
            String[] types = codecInfo.getSupportedTypes();
            for (String type : types) {
                Log.d(Recorder.class.getCanonicalName(), "Type: " + type);
                if (type.equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private MediaCodec configureMediaCodec() {
        MediaCodec codec;

        MediaCodecInfo info = selectCodec(mConfig.getMimeType());
        Log.e(Recorder.class.getCanonicalName(), "Codec: " + info.getName());

        MediaFormat format = MediaFormat.createVideoFormat(mConfig.getMimeType(),
                mConfig.getVideoWidth(), mConfig.getVideoHeight());
        format.setInteger(MediaFormat.KEY_PROFILE, 8);
        format.setInteger(MediaFormat.KEY_LEVEL, 0x40);

        // Set some required properties. The media codec may fail if these aren't defined.
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE, 6000000); // 6Mbps
        format.setFloat(MediaFormat.KEY_FRAME_RATE, mConfig.getFrameRate());
        format.setFloat(MediaFormat.KEY_CAPTURE_RATE, mConfig.getFrameRate());
        format.setFloat(MediaFormat.KEY_REPEAT_PREVIOUS_FRAME_AFTER,
                1000000 / mConfig.getFrameRate());
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1); // 1 seconds between I-frames

        try {
            codec = MediaCodec.createByCodecName(info.getName());
        } catch (IOException e) {
            Log.e(TAG, "Codec creation error", e);
            e.printStackTrace();
            releaseEncoders();
            return null;
        }

        codec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        return codec;
    }

    private void releaseEncoders() {
        Log.i(TAG, "Releasing encoders...");
        mDrainHandler.removeCallbacks(mDrainEncoderRunnable);
        if (mMediaMuxer != null) {
            Log.i(TAG, "Releasing the muxer...");
            if (mIsMuxerStarted) {
                mMediaMuxer.stop();
            }
            mMediaMuxer.release();
            mMediaMuxer = null;
            mIsMuxerStarted = false;
            Log.i(TAG, "Releasing the muxer... done");
        }
        if (mVideoEncoder != null) {
            Log.i(TAG, "Releasing the video encoder...");
            mVideoEncoder.stop();
            mVideoEncoder.release();
            mVideoEncoder = null;
            Log.i(TAG, "Releasing the video encoder... done");
        }
        if (mInputSurface != null) {
            Log.i(TAG, "Releasing the input surface...");
            mInputSurface.release();
            mInputSurface = null;
            Log.i(TAG, "Releasing the input surface... done");
        }

        mVideoBufferInfo = null;
        mDrainEncoderRunnable = null;
        mTrackIndex = -1;
        Log.i(TAG, "Releasing encoders... done");
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
    }

    /**
     * Stop the recording.
     * @throws RecorderException on errors.
     */
    public void stop() throws RecorderException {
        super.stop();
        if (mIsRecording) {
            Log.i(TAG, "Stopping...");
            try {
                mDrainHandler.removeCallbacks(mDrainEncoderRunnable);
                mMediaMuxer.stop();
                mVideoEncoder.stop();
                mCameraCaptureSession.stopRepeating();
                mInputSurface.release();
                mTrackIndex = -1;
                mMediaMuxer.release();
                mVideoBufferInfo = null;
                mIsRecording = false;
                mIsMuxerStarted = false;
                mCameraCaptureSession.close();
                mCameraCaptureSession = null;
            } catch (RuntimeException | CameraAccessException e) {
                Log.e(TAG, "Could not stop the recorder", e);
                throw new RecorderException(e.getMessage());
            }
            Log.i(TAG, "Stopping... done");
        }
    }

    @NotNull
    @Override
    public String toString() {
        return "#<Recorder " + mConfig.getName() + " (camera: " + mConfig.getMimeType() + ") "
                + this.hashCode() + ">";
    }

    /**
     * Close all acquired resources.
     */
    public void close() throws RecorderException {
        Log.i(TAG, "Closing...");
        stop();
        Log.i(TAG, "Closing... done");
    }
}
