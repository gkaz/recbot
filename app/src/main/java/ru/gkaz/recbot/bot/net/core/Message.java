package ru.gkaz.recbot.bot.net.core;

public interface Message {
    String RESPONSE       = "response";
    String RESULT         = "result";
    String RESULT_SUCCESS = "success";
    String RESULT_ERROR   = "error";

    /**
     * This method must return the message data as an array of bytes for transmitting over
     * a network.
     * @return array of bytes.
     */
    byte[] getContent();

    String toString();
}
