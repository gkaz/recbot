package ru.gkaz.recbot.bot;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import ru.gkaz.recbot.MainActivity;
import ru.gkaz.recbot.bot.core.Config;
import ru.gkaz.recbot.bot.core.Status;
import ru.gkaz.recbot.bot.event.Accelerometer;
import ru.gkaz.recbot.bot.event.EventListener;
import ru.gkaz.recbot.bot.event.EventSource;
import ru.gkaz.recbot.bot.event.EventSourceException;
import ru.gkaz.recbot.bot.net.Request;
import ru.gkaz.recbot.bot.net.RequestRm;
import ru.gkaz.recbot.bot.net.RequestPing;
import ru.gkaz.recbot.bot.net.RequestRegistration;
import ru.gkaz.recbot.bot.net.RequestSessionSend;
import ru.gkaz.recbot.bot.net.RequestSessionStart;
import ru.gkaz.recbot.bot.net.RequestSessionStop;
import ru.gkaz.recbot.bot.net.ResponseConf;
import ru.gkaz.recbot.bot.net.ResponseError;
import ru.gkaz.recbot.bot.net.ResponseHelp;
import ru.gkaz.recbot.bot.net.ResponseStatus;
import ru.gkaz.recbot.bot.net.ResponseSuccess;
import ru.gkaz.recbot.bot.net.FileMessage;
import ru.gkaz.recbot.bot.net.ResponseVersion;
import ru.gkaz.recbot.bot.net.core.Message;
import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.net.core.WebSocketAPI;
import ru.gkaz.recbot.bot.net.core.WebSocketEndpoint;
import ru.gkaz.recbot.bot.rec.Recorder;
import ru.gkaz.recbot.bot.rec.RecorderConfig;

public class RecBot {
    private static final String TAG = RecBot.class.getCanonicalName();
    private static final int WS_API_VERSION = 2;

    private Config mConfig;
    private EventSource mEventSource;
    private Device mDevice;
    private Session mSession;
    private SessionCompressor mSessionCompressor;
    private Simulcorder mSimulcorder;
    private CameraSwitcher mCameraSwitcher;
    private RecBotListener mListener;
    private WebSocketEndpoint mWebSocketEndpoint;
    private Context mContext;
    private String mDeviceID = Device.getDeviceID();
    private ScheduledExecutorService mScheduledExecutorService;
    private ScheduledFuture<?> mStatusUpdaterTask;

    private State mState = RecBot.State.STOPPED;

    public enum State {
        STOPPED,
        STARTED,
        STARTED_BY_A_TRIGGER,
        SENDING_DATA
    }

    private class RecBotAPI implements WebSocketAPI {
        @Override
        public void handleError(final String errorMessage) {
            Log.e(TAG, errorMessage);
            if (mConfig.isNetworkingEnabled()) {
                mListener.onDisconnection("WebSocket error: " + errorMessage);
            }
        }

        @Override
        public Message handleConnection(Map<String, List<String>> headers) {
            if (mConfig.isNetworkingEnabled()) {
                mListener.onConnection("Connected to a server: " + mConfig.loadServerURL());
            }
            Log.i(TAG, "Sending registration request... ");
            return new RequestRegistration(
                    mDevice.getDeviceID(),
                    getStatus());
        }

        public Message handleRequest(Request request) {
            String deviceID = request.getDeviceID();
            if ((! deviceID.equals("*")) && (! deviceID.equals(mDeviceID))) {
                return null;
            }
            JSONObject args = request.getArguments();
            switch (request.getCommand()) {
                case "version":
                    return new ResponseVersion(mDeviceID, WS_API_VERSION);

                case "start":
                    return handleStart(args);

                case "conf":
                    return handleConf(args);

                case "stop":
                    return handleStop();

                case "status":
                    return handleStatus();

                case "ls":
                    return handleLs(args);

                case "get":
                    return handleGet(args);

                case "rm":
                    return handleRm(args);

                case "help":
                    return handleHelp(args);

                default:
                    return new ResponseError(mDeviceID, "Unknown Command");
            }
        }
    }

    /**
     * Get the RecBot status;
     * @return a new status instance.
     */
    public Status getStatus() {
        Status status = new Status()
                .setDeviceID(mDevice.getDeviceID())
                .setDeviceAlias(mConfig.loadDeviceAlias())
                .setBatteryLevel(mDevice.getBatteryLevel())
                .setUptime(mDevice.getUptime())
                .setRecordingStatus(mSimulcorder.isRecording())
                .setRecordingsTotal(mSimulcorder.getRecordings());

        if (mConfig.loadCameraSwitchTimeout() > 0) {
            if (mCameraSwitcher != null) {
                status.setRecordingStatus(mCameraSwitcher.isRunning());
            } else {
                status.setRecordingStatus(false);
            }
        } else {
            status.setRecordingStatus(mSimulcorder.isRecording());
        }

        if (mSession != null) {
            String device = mConfig.loadCameraDevice(mCameraSwitcher.getCurrentDevice());
            status.setCurrentCamera(device);
            status.setRecordings(mSession.getSequenceNumber());
        } else {
            status.setRecordings(0);
            status.setCurrentCamera("front");
        }
        return status;
    }

    private Message handleStatus() {
        return new ResponseStatus(getStatus());
    }

    private Message handleStop() {
        stop();
        mListener.onSessionStop();
        return new ResponseSuccess(mDeviceID);
    }

    private Message handleLs(JSONObject args) {
        File[] list = getRecordingList();
        JSONArray response = new JSONArray();
        try {
            for (File f : list) {
                JSONObject elem = new JSONObject();
                elem.put("name", f.getName());
                if ((args.has("long?") && args.getBoolean("long?"))) {
                    elem.put("size", f.length());
                }
                response.put(elem);
            }
        } catch (JSONException e) {
            return new ResponseError(mDeviceID, e);
        }
        return new ResponseSuccess(mDeviceID, response);
    }

    private Message handleGet(JSONObject args) {
        if (! args.has("name")) {
            return new ResponseError(mDeviceID, "No file name provided.");
        }
        try {

            File file = new File(mSimulcorder.getMediaStorageDir().toString()
                    + "/" + args.getString("name"));
            return new FileMessage(file, Device.getDeviceID(), mConfig.loadDeviceAlias());
        } catch (JSONException e) {
            e.printStackTrace();
            return new ResponseError(mDeviceID, e);
        }
    }

    private Message handleStart(final JSONObject args) {
        try {
            JSONArray devices = args.getJSONArray("devices");
            start(devices, false);
            mWebSocketEndpoint.sendMessage(new ResponseSuccess(mDeviceID));
        } catch (JSONException e) {
            Log.w(TAG, e.getMessage());
            mWebSocketEndpoint.sendMessage(new ResponseError(mDeviceID, e));
        }
        return null;
    }

    private Message handleConf(final JSONObject args) {
        if (args.length() == 0) {
            return new ResponseConf(mDeviceID, mConfig);
        }

        try {
            JSONObject conf = args.getJSONObject(Config.KEY);
            mConfig.fromJSON(conf);
            mWebSocketEndpoint.sendMessage(new ResponseSuccess(mDeviceID));
            mListener.onReconfigure();
        } catch (JSONException e) {
            return new ResponseError(mDeviceID, e);
        }

        return new ResponseSuccess(mDeviceID);
    }

    private Message handleRm(final JSONObject args) {
        if (args.length() == 0) {
            return new ResponseError(
                    mDeviceID,
                    "Insufficient number of arguments (expecting one.)");
        }
        if (args.has("name")) {
            try {
                String name = args.getString("name");
                if (name.equals("*")) {
                    File[] list = getRecordingList();
                    for (File f : list) {
                        f.delete();
                    }
                    return new ResponseSuccess(mDeviceID);
                } else {
                    File file = new File(mSimulcorder.getMediaStorageDir().toString()
                            + "/" + name);
                    return file.delete()
                            ? new ResponseSuccess(mDeviceID)
                            : new ResponseError(mDeviceID, "Could not delete the file");
                }
            } catch (JSONException e) {
                return new ResponseError(mDeviceID, e);
            }
        } else {
            return new ResponseError(mDeviceID, "'name' parameter not found.");
        }
    }

    private Message handleHelp(final JSONObject args) {
        if ((args == null) || (args.length() == 0)) {
            return new ResponseHelp(mDeviceID);
        }
        String command;
        try {
            command = args.getString("command");
        } catch (JSONException e) {
            e.printStackTrace();
            return new ResponseError(mDeviceID, e);
        }
        return new ResponseHelp(command);
    }

    public RecBot(Context context, RecBotListener listener) {
        mListener = listener;
        mContext  = context;
        mConfig = new Config(context);
    }

    public void init() {
        String serverURL = mConfig.loadServerURL();
        if (serverURL == null) {
            mListener.onConfigure();
            return;
        }

        Log.i(TAG, "Server URL: " + serverURL);

        mSessionCompressor = new SessionCompressor(
                new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MOVIES),
                        "RecBot"));

        mDevice = new Device(mContext);


        String cameraFront = mConfig.loadCameraFrontID();
        Log.i(TAG, "Frontal camera: " + cameraFront);

        String cameraBack =  mConfig.loadCameraRearID();
        Log.i(TAG, "Rear camera: " + cameraFront);

        mSimulcorder = new Simulcorder(
                mContext,
                new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MOVIES), "RecBot"));

        try {
            String[] frontalResolution = mConfig.loadFrontalResolution().split("x");
            String[] rearResolution = mConfig.loadRearResolution().split("x");

            mSimulcorder.createRecorder(
                    new RecorderConfig()
                            .setCameraId(cameraFront)
                            .setMimeType(mConfig.loadFrontalFormat())
                            .setName("FRONT")
                            .setVideoDimensions(
                                    Integer.parseInt(frontalResolution[0]),
                                    Integer.parseInt(frontalResolution[1]))
                            .setFrameRate(mConfig.loadFrontalFrameRate()),
                    mConfig.loadFrontalRecorderType());

            mSimulcorder.createRecorder(
                    new RecorderConfig()
                            .setCameraId(cameraBack)
                            .setMimeType(mConfig.loadRearFormat())
                            .setName("REAR")
                            .setVideoDimensions(
                                    Integer.parseInt(rearResolution[0]),
                                    Integer.parseInt(rearResolution[1]))
                            .setFrameRate(mConfig.loadRearFrameRate()),
                    mConfig.loadRearRecorderType());

        } catch (Recorder.RecorderException e) {
            Log.e(TAG, e.getMessage());
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage(e.getMessage());
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            mSimulcorder = null;
                        }
                    });
            builder.create().show();
        }

        mWebSocketEndpoint = new WebSocketEndpoint(new RecBotAPI());
        if ((serverURL != null) && mConfig.isNetworkingEnabled()) {
            mWebSocketEndpoint.connect(serverURL);
            mWebSocketEndpoint.run();
        } else {
            mListener.onDisconnection("Networking is disabled.");
        }

        float movementDetectionThreshold = mConfig.loadMovementDetectionThreshold();
        if (movementDetectionThreshold > 0) {
            mListener.onReady(mDeviceID, mConfig.loadDeviceAlias());
            mEventSource = new Accelerometer(mContext);
            mEventSource.setThreshold(movementDetectionThreshold);
            try {
                mEventSource.open();
            } catch (EventSourceException e) {
                Log.e(TAG, e.getMessage());
            }

            mEventSource.setListener(new EventListener() {
                @Override
                public boolean onStart() {
                    if (mState == State.STOPPED) {
                        JSONArray devices = new JSONArray();
                        devices.put("front");
                        devices.put("rear");
                        start(devices, true);
                        return true;
                    }
                    return false;
                }

                @Override
                public boolean onStop() {
                    if (mState == State.STARTED_BY_A_TRIGGER) {
                        stop();
                        return true;
                    } else {
                        Log.i(TAG, "onStop: Ignoring event");
                        return false;
                    }
                }

                @Override
                public boolean isStarted() {
                    return mSimulcorder.isRecording();
                }
            });
        }
        mScheduledExecutorService = Executors.newScheduledThreadPool(5);
        mStatusUpdaterTask = mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                mListener.onStatus(getStatus());
            }
        }, 0,10, TimeUnit.SECONDS);
    }

    private File[] getRecordingList() {
        return mSimulcorder.getMediaStorageDir().listFiles();
    }

    private void sendRecordedData() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File zipFile = mSessionCompressor.compress(mSession,
                            mConfig.loadAutoRemoveData());
                    Log.d(TAG, "Compressed file: " + zipFile);
                    mWebSocketEndpoint.sendMessage(new FileMessage(zipFile, mSession));
                    if (mConfig.loadAutoRemoveData()) {
                        zipFile.delete();
                    }
                    mListener.onNotify("Saving and sending recording session... done");
                    mWebSocketEndpoint.sendMessage(new RequestSessionStop(mSession, getStatus()));
                    mState = State.STOPPED;
                    mListener.onSessionStop();
                    mSession = null;
                    mListener.onStatus(getStatus());
                } catch (IOException e) {
                    Log.e(TAG, "Could not compress the session data: " + e.getMessage());
                }
            }
        });
        thread.start();
    }

    public void start() {
        JSONArray devices = new JSONArray();
        devices.put("front");
        devices.put("rear");
        start(devices, false);
        mListener.onStatus(getStatus());
    }

    public void start(JSONArray devices, boolean isTriggered) {
        Long cameraSwitchTimeout = new Long(mConfig.loadCameraSwitchTimeout());
        if (devices.length() == 1) {
            cameraSwitchTimeout = new Long(0);
        }
        mSession = new Session(
                mDevice.getDeviceID(),
                mConfig.loadDeviceAlias(),
                System.currentTimeMillis());
        if (cameraSwitchTimeout > 0) {
            String[] deviceArr = new String[2];
            try {
                deviceArr[0] = mConfig.loadCameraID(devices.getString(0));
                deviceArr[1] = mConfig.loadCameraID(devices.getString(1));
            } catch (JSONException e) {
                Log.e(TAG, e.getLocalizedMessage());
                // TODO: Return an error message to the connected server.
                return;
            }
            if ((deviceArr[0] == null) || (deviceArr[1] == null)) {
                // TODO: Return an error message to the connected server.
                return;
            }

            mCameraSwitcher = new CameraSwitcher(
                    this,
                    mSession,
                    mSimulcorder,
                    mWebSocketEndpoint,
                    deviceArr,
                    cameraSwitchTimeout);

            mCameraSwitcher.start();
            mWebSocketEndpoint.sendMessage(new RequestSessionStart(mSession, getStatus()));
            mListener.onSessionStart();
            mState = isTriggered ? State.STARTED_BY_A_TRIGGER : State.STARTED;
        } else {
            try {
                if ((devices != null) && (devices.length() > 0)) {
                    String[] arr = new String[devices.length()];
                    for (int idx = 0; idx < devices.length(); ++idx) {
                        arr[idx] = mConfig.loadCameraID(devices.getString(idx));
                    }
                    mSimulcorder.start(mSession, Simulcorder.MediaType.VIDEO, arr);
                } else {
                    mSimulcorder.start(mSession, Simulcorder.MediaType.VIDEO);
                }
                mListener.onSessionStart();
                mState = isTriggered ? State.STARTED_BY_A_TRIGGER : State.STARTED;
            } catch (Simulcorder.SimulcorderException | JSONException e) {
                Log.e(TAG, e.getMessage());
                mSimulcorder.stop();
                mListener.onSessionStop();
                mState = State.STOPPED;
            }
        }
    }

    public void stop() {
        Log.i(TAG, "Stopping...");
        if (mSession != null) {
            mListener.onNotify(new RequestSessionStop(mSession, getStatus()).toString());
            if (mCameraSwitcher == null) {
                Log.d(TAG, "Stopping the simulcorder...");
                mSimulcorder.stop();
            } else {
                Log.d(TAG, "Stopping the camera switcher...");
                mCameraSwitcher.stop();
            }
            if (mConfig.loadAutoSendData()) {
                Log.d(TAG, "Sending the data...");
                mState = State.SENDING_DATA;
                mListener.onSessionSend();
                mWebSocketEndpoint.sendMessage(new RequestSessionSend(mSession, getStatus()));
                sendRecordedData();
            } else {
                Log.d(TAG, "Stopping the session...");
                mWebSocketEndpoint.sendMessage(new RequestSessionStop(mSession, getStatus()));
                mState = State.STOPPED;
                mListener.onSessionStop();
                mSession = null;
                mListener.onStatus(getStatus());
            }
        }
        mStatusUpdaterTask.cancel(false);
        Log.d(TAG, "[" + mState.toString() + "]" + " -> [STOPPED]");
        mState = State.STOPPED;
    }

    public State getState() {
        return mState;
    }

    public void destroy() {
        stop();
        mCameraSwitcher = null;
        if (mSimulcorder != null) {
            mSimulcorder.close();
        }

        if (mWebSocketEndpoint != null) {
            Log.d(TAG, "Deleting the WebSocket endpoint...");
            mWebSocketEndpoint.disconnect();
            mWebSocketEndpoint = null;
        }
    }

    public void clear() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", "*");
            handleRm(obj);
            if (mWebSocketEndpoint != null) {
                mWebSocketEndpoint.sendMessage(new RequestRm(Device.getDeviceID()));
            }
        } catch (JSONException e) {
            Log.e(MainActivity.class.getSimpleName(), e.getMessage());
        }
    }

    public void ping() {
        if (mWebSocketEndpoint != null) {
            mWebSocketEndpoint.sendMessage(new RequestPing(Device.getDeviceID()));
        } else {
            Log.e(TAG, "mWebSocketEndpoint is not created yet!");
        }
    }
}
