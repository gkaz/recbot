package ru.gkaz.recbot.bot;

import android.content.Context;
import android.os.BatteryManager;
import android.os.SystemClock;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.content.Context.BATTERY_SERVICE;

public class Device {
    private Context mContext;
    public Device(Context context) {
        mContext = context;
    }

    public int getBatteryLevel() {
        BatteryManager bm = (BatteryManager) mContext.getSystemService(BATTERY_SERVICE);
        return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
    }

    public long getUptime() {
        return SystemClock.elapsedRealtime();
    }

    /**
     * Get the MAC address of the device.
     * @return MAC address as a string, or empty string on errors.
     */
    public static String getMACAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

    public static String getDeviceID() {
        return getMACAddress().replace(":", "");
    }
}
