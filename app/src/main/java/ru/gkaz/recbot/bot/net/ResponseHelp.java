package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ResponseHelp extends ResponseSuccess {
    public static final String HELP_KEY        = "help";
    public static final String DESCRIPTION_KEY = "description";
    public static final String EXAMPLES_KEY    = "examples";
    private JSONObject mHelpObject = new JSONObject();
    private JSONArray mExamples = new JSONArray();

    public ResponseHelp(String deviceID) {
        this(deviceID, "help");
    }

    public ResponseHelp(String deviceID, String command) {
        super(deviceID);
        String description = "";
        try {
            switch (command) {
                case "ls":
                    description = "Get the list of recorded files.";
                    break;

                case "start":
                    description = "Start video recording.";
                    break;

                case "stop":
                    description = "Stop video recording.";
                    break;

                case "get":
                    description = "Get the file from the device.";
                    break;

                case "rm":
                    description = "Remove a file (or files) from the device.";
                    break;

                case "conf":
                    description = "Get or set the device configuration.";
                    break;

                case "version":
                    description = "Get the API version.";
                    break;

                case "help":
                default:
                    description = "Get the description of an API command."
                            + " Available commands:"
                            + " start, stop, ls, get, rm, conf, help, version";
                    break;
            }
            mHelpObject.put(DESCRIPTION_KEY, description);
            mHelpObject.put(EXAMPLES_KEY, mExamples);
            put(HELP_KEY, mHelpObject);
        } catch (JSONException e) {
            Log.e(ResponseHelp.class.getSimpleName(), e.getMessage());
        }
    }
}
