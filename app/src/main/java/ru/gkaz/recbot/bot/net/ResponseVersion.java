package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;

public class ResponseVersion extends ResponseSuccess {
    public static final String VERSION_KEY = "version";
    public ResponseVersion(String deviceID, int version) {
        super(deviceID);
        try {
            put(VERSION_KEY, version);
        } catch (JSONException e) {
            Log.e(ResponseVersion.class.getSimpleName(), e.getMessage());
        }
    }
}
