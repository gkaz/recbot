package ru.gkaz.recbot.bot.event;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

public class Accelerometer extends EventSource {
    private static final String TAG = Accelerometer.class.getSimpleName();
    private static SensorManager mSensorManager;
    private Sensor mSensor;
    private boolean mIsRunning = false;
    private State mState = State.IDLE;
    private static final long INTERVAL = 500000000; // nanoseconds

    private enum State {
        GET_FIRST_MEASUREMENT,
        IDLE,
        MOVEMENT,
        STOP_CHECK
    }

    public Accelerometer(Context context) {
        super(context);
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    public void open() throws EventSourceException {
        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_LINEAR_ACCELERATION);
        if (sensors.size() == 0) {
            throw new EventSourceException("Accelerometer is not available.");
        }
        mSensor = sensors.get(0);

        mIsRunning = mSensorManager.registerListener(new SensorEventListener() {
            private long mNow = 0; // nanoseconds
            private long mLastUpdate = 0;
            private long mInactivityStart = 0;
            private float mX = 0;
            private float mY = 0;
            private float mZ = 0;
            private float mLastX = 0;
            private float mLastY = 0;
            private float mLastZ = 0;
            private static final int SAMPLES = 20;
            private double mSamples[] = new double[SAMPLES];
            private int mIdx = 0;

            private float calculateForce(SensorEvent event) {
                mX = event.values[0];
                mY = event.values[1];
                mZ = event.values[2];
                return Math.abs(mX + mY + mZ - mLastX - mLastY - mLastZ);
            }

            private float truncatedMean(double[] samples) {
                float force = 0;
                Arrays.sort(samples);
                for (int i = 1; i < samples.length - 1; ++i) {
                    force += samples[i];
                }
                force /= samples.length - 2;
                return force;
            }

            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() != Sensor.TYPE_LINEAR_ACCELERATION){
                    return;
                }
                mNow = event.timestamp;
                switch (mState) {
                    case GET_FIRST_MEASUREMENT:
                        mSamples[mIdx] = calculateForce(event);
                        mIdx++;
                        mLastX = mX;
                        mLastY = mY;
                        mLastZ = mZ;
                        mState = State.IDLE;
                        mLastUpdate = mNow;
                        break;

                    case IDLE:
                        if ((mNow - mLastUpdate) > INTERVAL) {
                            mSamples[mIdx] = calculateForce(event);
                            mIdx++;
                            mLastX = mX;
                            mLastY = mY;
                            mLastZ = mZ;
                            if (mIdx == mSamples.length) {
                                float force = truncatedMean(mSamples);
                                Log.d(TAG, "    [IDLE] force: " + force);
                                if (Double.compare(force, mThreshold) > 0) {
                                    Log.e(TAG, "*** MOVEMENT ***");
                                    if (mListener.onStart()) {
                                        mState = State.MOVEMENT;
                                    } else {
                                        Log.d(TAG, "    [IDLE] event ignored");
                                    }
                                }
                                mIdx = 0;
                            }

                            //Log.d(TAG, "    x: " + mX + ", y: " + mY + ", z: " + mZ);
                            mLastUpdate = mNow;
                        }
                        break;

                    case MOVEMENT:
                        if ((mNow - mLastUpdate) > INTERVAL) {
                            mSamples[mIdx] = calculateForce(event);
                            mIdx++;
                            mLastX = mX;
                            mLastY = mY;
                            mLastZ = mZ;
                            if (mIdx == mSamples.length) {
                                float force = truncatedMean(mSamples);
                                Log.d(TAG, "    [MOVEMENT] force: " + force);
                                if (Double.compare(force, mThreshold) <= 0) {
                                    Log.e(TAG, "*** STOP_CHECK ***");
                                    mState = State.STOP_CHECK;
                                    mInactivityStart = mNow;
                                }
                                mIdx = 0;
                            }
                            mLastUpdate = mNow;
                        }
                        break;

                    case STOP_CHECK:
                        if ((mNow - mLastUpdate) > INTERVAL) {
                            mSamples[mIdx] = calculateForce(event);
                            mIdx++;
                            mLastX = mX;
                            mLastY = mY;
                            mLastZ = mZ;
                            if (mIdx == mSamples.length) {
                                float force = truncatedMean(mSamples);
                                mIdx = 0;
                                Log.d(TAG, "    [STOP_CHECK] force: " + force);
                                if (Double.compare(force, mThreshold) <= 0) {
                                    if ((mNow - mInactivityStart) > (INTERVAL * 2)) {
                                        if (mListener.onStop()) {
                                            Log.e(TAG, "*** IDLE ***");
                                            mInactivityStart = 0;
                                            mState = State.IDLE;
                                        } else {
                                            Log.e(TAG, "*** MOVEMENT ***");
                                            mState = State.MOVEMENT;
                                        }
                                    }
                                } else {
                                    Log.e(TAG, "*** MOVEMENT ***");
                                    mState = State.MOVEMENT;
                                }
                            }
                            mLastUpdate = mNow;
                        }
                        break;

                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        }, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void close() throws EventSourceException {

    }

}
