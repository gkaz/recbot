package ru.gkaz.recbot.bot.core;

import org.json.JSONObject;

/**
 * This interface describes a generic RecBot core type.
 */
public interface Type {
    /**
     * Get the type key that must be used to store a JSON object returned by <code>toJSON</code>
     * method in a larger JSON object.
     * @return a type key.
     */
    String getKey();

    /**
     * Convert the type to a JSON object that can be sent over a network to the server.
     * @return a new JSON object with the serialized type content.
     */
    JSONObject toJSON();
}
