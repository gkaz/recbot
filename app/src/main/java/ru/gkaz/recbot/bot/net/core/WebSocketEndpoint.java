package ru.gkaz.recbot.bot.net.core;

import android.util.Log;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ru.gkaz.recbot.bot.net.Request;
import ru.gkaz.recbot.bot.net.FileMessage;

/**
 * This class describes a WebSocket endpoint.
 */
public class WebSocketEndpoint {
    private final static String TAG = WebSocketEndpoint.class.getSimpleName();
    private final static int BUFFER_SIZE = 65536;
    private final WebSocketAPI api;
    private WebSocket    ws;

    private Adapter mAdapter;
    private String mServerURL;
    private Timer mTimer;
    private State mState = State.CREATED;

    public enum State {
        CREATED,
        CONNECTED,
        DISCONNECTED,
        STOPPED
    }

    private class Adapter extends WebSocketAdapter {
        @Override
        public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame,
                                   WebSocketFrame clientCloseFrame, boolean closedByServer)
                throws Exception {
            super.onDisconnected(websocket, serverCloseFrame, clientCloseFrame, closedByServer);
            Log.w(TAG, "Disconnected");
            switch (mState) {
                case STOPPED:
                    return;

                default:
                    mState = State.DISCONNECTED;
                    reconnect();
            }
        }

        @Override
        public void onConnected(WebSocket websocket, Map<String, List<String>> headers)
                throws Exception {
            super.onConnected(websocket, headers);
            Log.i(TAG, "onConnected: connected");
            switch (mState) {
                case CONNECTED:
                    break;

                case STOPPED:
                    disconnect();
                    break;

                case CREATED:
                case DISCONNECTED: {
                    Log.i(TAG, "onConnected: Calling API method...");
                    mState = State.CONNECTED;
                    Message response = api.handleConnection(headers);
                    if (response != null) {
                        Log.i(TAG, "onConnected: Sending a message: " + response.toString());
                        sendMessage(response);
                    }

                    break;
                }
            }
        }

        @Override
        public void onConnectError(WebSocket websocket, WebSocketException exception)
                throws Exception {
            super.onConnectError(websocket, exception);
            Log.e(TAG, "onConnectError: " + exception.getLocalizedMessage());
            switch (mState) {
                case CREATED:
                    break;

                case DISCONNECTED:
                case CONNECTED:
                    reconnect();
                    api.handleError(exception.getMessage());
            }
        }

        @Override
        public void onTextMessage(WebSocket websocket, String message) throws Exception {
            Request request = new Request(message);
            Log.d(TAG, "Message: " + message);
            Message response = api.handleRequest(request);
            if (response != null) {
                Log.d(TAG, "Response: " + response);
                sendMessage(response);
            }
        }
    }

    /**
     * Try to reconnect to a WebSocket server with 5ms delay.
     */
    private void reconnect() {
        if (mState != State.STOPPED) {
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    connect();
                }
            }, 5000);
        }
    }

    public WebSocketEndpoint(WebSocketAPI api) {
        this.api = api;
        mAdapter = new Adapter();
    }

    private void connect() {
        if (ws != null) {
            ws.flush();
            ws.clearListeners();
            ws.disconnect();
            ws = null;
        }

        WebSocketFactory factory = new WebSocketFactory().setConnectionTimeout(5000);

        try {
            ws = factory.createSocket(mServerURL);
            ws.setFrameQueueSize(1);
            ws.addListener(mAdapter);
        } catch (IOException | IllegalArgumentException e) {
            Log.e(TAG, "connect: "+ e.getLocalizedMessage());
            api.handleError(e.getMessage());
        }

        ws.connectAsynchronously();
    }

    /**
     * Connect to a WebSocket server.
     * @param serverURL -- Server URL to connect to.
     */
    public void connect(String serverURL) {
        mState = State.DISCONNECTED;
        mServerURL = serverURL;
        connect();
    }

    /**
     * Run the endpoint.
     */
    public void run() {
        // TODO: Remove.
    }

    /**
     * Disconnect from the WebSocket server.
     */
    public void disconnect() {
        if (isConnected()) {
            ws.flush();
            ws.removeListener(mAdapter);
            ws.disconnect();
            ws = null;
        }
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        mState = State.DISCONNECTED;
    }

    /**
     * Send a file from a fileMessage as a sequence of zipped packets.  Each ZIP archive contains
     * one chunk of a file, with the following naming schema:
     *     deviceId_timestamp_totalFileLength_chunkCount_currentChunkNumber
     *
     * @param fileMessage A FileMessage instance with a file to send.
     */
    private void sendFile(FileMessage fileMessage) {
        byte[] buffer = new byte[BUFFER_SIZE];
        File file = fileMessage.getFile();
        Log.i(TAG, "Sending file: " + file);
        try {
            FileInputStream fis = new FileInputStream(file);
            int size;
            int counter = 1;
            byte[] bufferPart;
            String deviceID = fileMessage.getDeviceID();
            int chunkCount = (int) Math.ceil((double) file.length() / (double) BUFFER_SIZE);
            long timestamp = fileMessage.getSession() != null
                    ? fileMessage.getSession().getTimestamp()
                    : System.currentTimeMillis();

            Log.i(TAG, String.format(Locale.ENGLISH,
                    "Device ID: %s; chunks: %d, timestamp: %d",
                    deviceID, chunkCount, timestamp));

            String prefix = String.format(
                    Locale.ENGLISH,
                    "%s_%d_%08d",
                    deviceID,
                    timestamp,
                    chunkCount);

            while ((size = fis.read(buffer)) > 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ZipOutputStream zos = new ZipOutputStream(baos);
                zos.setLevel(0);
                zos.putNextEntry(new ZipEntry(String.format(Locale.ENGLISH,
                        "%s_%08d", prefix, counter)));

                if (size < BUFFER_SIZE) {
                    bufferPart = Arrays.copyOfRange(buffer, 0, size);
                    zos.write(bufferPart);
                } else {
                    zos.write(buffer);
                }

                Log.d(TAG, String.format(Locale.ENGLISH, "%5d byte(s) sent (%08d/%08d)",
                        size, counter, chunkCount));

                zos.closeEntry();
                zos.close();

                ws.sendBinary(baos.toByteArray());
                ws.flush();

                counter++;

                if (size < BUFFER_SIZE) {
                    break;
                }
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return mState == State.CONNECTED;
    }

    /**
     * Send message to the socket.
     * @param msg FileMessage to send.
     */
    public void sendMessage(Message msg) {
        if (isConnected()) {
            if (msg instanceof FileMessage) {
                sendFile((FileMessage) msg);
            } else {
                ws.sendText(msg.toString());
            }
        }
    }
}
