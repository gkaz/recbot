package ru.gkaz.recbot.bot.net;

import org.json.JSONException;

import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.core.Status;

public class RequestSessionSend extends RequestSession {
    public static final String RECORDINGS_KEY = "recordings";
    public RequestSessionSend(Session session, Status status) {
        super(RequestSession.ACTION_SEND, session, status);
        try {
            put(RECORDINGS_KEY, session.getSequenceNumber());
        } catch (JSONException e) {
            // TODO:
        }
    }
}
