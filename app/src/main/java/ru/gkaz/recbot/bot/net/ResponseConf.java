package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.gkaz.recbot.bot.core.Config;

public class ResponseConf extends ResponseSuccess {
    public ResponseConf(String deviceID, Config config) {
        super(deviceID);
        try {
            put(Config.KEY, config.toJSON());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(ResponseConf.class.getSimpleName(), e.getLocalizedMessage());
        }
    }
}
