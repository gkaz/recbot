package ru.gkaz.recbot.bot;

import ru.gkaz.recbot.bot.core.Status;

public interface RecBotListener {
    /**
     * This method is called when networking is enabled and there is a connection error.
     * @param message
     */
    void onDisconnection(String message);

    /**
     * This method is called when networking is enabled and a connection is established.
     * @param message
     */
    void onConnection(String message);

    void onNotify(final String message);
    void onSessionStart();
    void onSessionSend();
    void onSessionStop();
    void onConfigure();
    void onReconfigure();
    void onReady(String deviceID, String deviceAlias);
    void onStatus(Status status);
}
