package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import ru.gkaz.recbot.bot.net.core.Message;

/**
 * This class describes a generic response on a command from a server.
 */
public class Response extends MessageJSON {
    public static final String DEVICE_ID_KEY    = "device-id";
    public Response(String deviceID, String result) {
        this(deviceID, result, new JSONObject());
    }
    public Response(String deviceID, String result, JSONObject object) {
        super(object);
        setResult(result);
        try {
            put(DEVICE_ID_KEY, deviceID);
        } catch (JSONException e) {
            Log.e(MessageJSON.class.getSimpleName(), e.getMessage());
        }
    }

    protected void setResult(String result) {
        try {
            mObject.put(Message.RESULT, result);
        } catch (JSONException e) {
            Log.e(MessageJSON.class.getSimpleName(), e.getMessage());
        }
    }
}
