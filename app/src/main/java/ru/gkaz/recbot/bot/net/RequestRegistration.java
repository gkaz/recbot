package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;

import ru.gkaz.recbot.bot.core.Status;

public class RequestRegistration extends Request {
    public static final String REGISTER = "register";
    public RequestRegistration(String deviceID, Status status) {
        super(deviceID, REGISTER);
        try {
            put(Status.KEY, status.toJSON());
        } catch (JSONException e) {
            Log.e(RequestSession.class.getSimpleName(), e.getMessage());
        }
    }
}
