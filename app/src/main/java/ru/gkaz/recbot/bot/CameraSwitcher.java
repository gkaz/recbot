package ru.gkaz.recbot.bot;

import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import ru.gkaz.recbot.bot.net.RequestSwitchCamera;
import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.net.core.WebSocketEndpoint;

public class CameraSwitcher {
    private RecBot mRecBot;
    private String[] mDevices;
    private Simulcorder mSimulcorder;
    private WebSocketEndpoint mWebSocketEndpoint;
    private Long mTimeout;
    private int mCurrentDevice;
    private Session mSession;
    private boolean mIsRunning = false;
    private ScheduledExecutorService mScheduledExecutorService;
    private ScheduledFuture<?> mTask;

    /**
     *
     * @param session Recording session.
     * @param simulcorder
     * @param wsEndpoint  WebSocketEndpoint instance.  If not null, the CameraSwitcher will send
     *                    messages to the endpoint on camera switch.
     * @param devices
     * @param timeout Timeout in seconds.
     */
    public CameraSwitcher(RecBot recBot,
                          Session session,
                          Simulcorder simulcorder,
                          WebSocketEndpoint wsEndpoint,
                          String[] devices,
                          Long timeout) {
        mRecBot  = recBot;
        mDevices = devices;
        mSimulcorder = simulcorder;
        mWebSocketEndpoint = wsEndpoint;
        mTimeout = timeout;
        mSession = session;
        mScheduledExecutorService = Executors.newScheduledThreadPool(5);
    }

    public int getCurrentDevice() {
        return mCurrentDevice;
    }

    public void start() {
        if (mIsRunning) {
            Log.w(CameraSwitcher.class.getSimpleName(), "Already running");
            return;
        }

        mIsRunning = true;

        mCurrentDevice = 1;
        mTask = mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                mSimulcorder.stop();

                if (! mIsRunning) {
                    mTask.cancel(false);
                    return;
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (mWebSocketEndpoint != null) {
                    mWebSocketEndpoint.sendMessage(
                            new RequestSwitchCamera(mSession, mRecBot.getStatus()));
                }

                try {
                    mSimulcorder.start(
                            mSession,
                            Simulcorder.MediaType.VIDEO,
                            new String[] { mDevices[mCurrentDevice] });
                } catch (Simulcorder.SimulcorderException e) {
                    Log.e(CameraSwitcher.class.getSimpleName(), e.getMessage());
                    mIsRunning = false;
                    return;
                }

                if (mCurrentDevice == 0) {
                    mCurrentDevice = 1;
                } else {
                    mCurrentDevice = 0;
                }
            }
        }, 0, mTimeout, TimeUnit.SECONDS);
    }

    public void stop() {
        mIsRunning = false;
    }

    public boolean isRunning() {
        return mIsRunning;
    }
}
