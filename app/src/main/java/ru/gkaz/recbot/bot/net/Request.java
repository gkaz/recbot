package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Request extends MessageJSON {
    public static final String DEVICE_ID_KEY    = "device-id";
    public static final String COMMAND_KEY      = "command";
    public static final String COMMAND_ARGS_KEY = "arguments";

    public Request(String deviceID, String command) {
        try {
            put(DEVICE_ID_KEY, deviceID);
            put(COMMAND_KEY, command);
        } catch (JSONException e) {
            Log.e(Request.class.getSimpleName(), e.getMessage());
        }
    }

    public Request(String jsonString) throws JSONException {
        super(jsonString);
    }

    /**
     * Get the device ID from the request.
     * @return Device ID as a string.
     */
    public String getDeviceID() {
        try {
            return (String) get(DEVICE_ID_KEY);
        } catch (JSONException e) {
            Log.e(Request.class.getSimpleName(), e.getMessage());
            return null;
        }
    }

    /**
     * Get the request command.
     * @return Request command as a string.
     */
    public String getCommand() {
        try {
            return (String) get(COMMAND_KEY);
        } catch (JSONException e) {
            Log.e(Request.class.getSimpleName(), e.getMessage());
            return null;
        }
    }

    /**
     * Get the request arguments.
     * @return The request arguments as a JSON object.
     */
    public JSONObject getArguments() {
        try {
            return (JSONObject) get(COMMAND_ARGS_KEY);
        } catch (JSONException e) {
            Log.e(Request.class.getSimpleName(), e.getMessage());
            return null;
        }
    }

    /**
     * SEt the request arguments.
     * @param args A JSON object with arguments.
     */
    public void setArguments(JSONObject args) {
        try {
            put(COMMAND_ARGS_KEY, args);
        } catch (JSONException e) {
            Log.e(Request.class.getSimpleName(), e.getMessage());
        }
    }
}
