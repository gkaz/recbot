package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;

import ru.gkaz.recbot.bot.net.core.Message;

public class ResponseError extends Response {
    public ResponseError(String deviceID, String message) {
        super(deviceID, Message.RESULT_ERROR);
        try {
            put("message", message);
        } catch (JSONException e) {
            Log.e(ResponseError.class.getSimpleName(), e.getMessage());
        }
    }

    public ResponseError(String deviceID, Exception exception) {
        this(deviceID, exception.getMessage());
    }
}
