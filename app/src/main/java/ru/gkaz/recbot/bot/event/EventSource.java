package ru.gkaz.recbot.bot.event;

import android.content.Context;

/**
 * Abstract source of events that could trigger recording.
 */
public abstract class EventSource {
    protected Context mContext;
    protected EventListener mListener;

    /**
     * A threshold that controls when event trigger the recording stop/start.
     */
    protected float mThreshold;

    public EventSource(Context context) {
        mContext = context;
    }

    /**
     * Open an event source (e.g. accelerometer.)
     * @throws EventSourceException
     */
    public abstract void open() throws EventSourceException;

    /**
     * Close the opened event source and free the resources.
     * @throws EventSourceException
     */
    public abstract void close() throws EventSourceException;

    public void setThreshold(float value) {
        mThreshold = value;
    }

    public void setListener(EventListener listener) {
        mListener = listener;
    }
}
