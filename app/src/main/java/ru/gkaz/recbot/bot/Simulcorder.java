package ru.gkaz.recbot.bot;

import android.content.Context;
import android.hardware.camera2.CameraDevice;
import android.util.Log;
import android.view.Surface;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.rec.AbstractRecorder;
import ru.gkaz.recbot.bot.rec.Recorder;
import ru.gkaz.recbot.bot.rec.RecorderConfig;
import ru.gkaz.recbot.bot.rec.RecorderSimple;

/**
 * This class represents a RecBot simulcorder that allows to record data from several recorder
 * instances simultaneously.
 */
public class Simulcorder {
    private final static String TAG = "recbot.RecBot";

    private HashMap<Integer, AbstractRecorder> mRecorders;
    private State mState = State.STOPPED;
    private File mMediaStorageDir;
    private Context mContext;
    private Session mSession;
    private int mRecordings = 0;

    public enum State {
        RECORDING,
        STOPPED,
        CLOSED
    }

    public enum MediaType {
        IMAGE,
        VIDEO
    }

    public class SimulcorderException extends Exception {
        public SimulcorderException(String message) {
            super(message);
        }
    }

    public Simulcorder(Context context, File mediaStorageDir) {
        this.mContext = context;
        mRecorders = new HashMap<>();
        this.mMediaStorageDir = mediaStorageDir;
    }

    public State getState() {
        return mState;
    }

    public boolean isRecording() {
        return mState == State.RECORDING;
    }

    public File getMediaStorageDir() {
        return mMediaStorageDir;
    }

    /**
     * Create a new Recorder instance and store it in the Simulcorder.
     * @param config A Recorder configuration.
     * @throws Recorder.RecorderException on errors.
     */
    public void createRecorder(RecorderConfig config, boolean useOldRecorder)
            throws AbstractRecorder.RecorderException {
        if (! useOldRecorder) {
            mRecorders.put(Integer.parseInt(config.getCameraId()), new Recorder(mContext, config));
        } else {
            mRecorders.put(Integer.parseInt(config.getCameraId()), new RecorderSimple(config));
        }
    }

    public void  addRecorder(int cameraID, AbstractRecorder recorder) {
        mRecorders.put(cameraID, recorder);
    }

    public AbstractRecorder getRecorder(int cameraID) {
        return mRecorders.get(cameraID);
    }

    /**
     * Get camera by its ID.
     * @param cameraID
     * @return
     */
    public CameraDevice getCamera(int cameraID) {
        if (mRecorders.get(cameraID) instanceof Recorder) {
            return ((Recorder) mRecorders.get(cameraID)).getCameraDevice();
        } else {
            return null;
        }
    }

    /**
     * Start recording.
     * @param recordingType Type of the recording to make.
     * @throws Recorder.RecorderException
     */
    public void start(Session session, MediaType recordingType) {
        mState = State.RECORDING;
        mSession = session;
        for (AbstractRecorder recorder : mRecorders.values()) {
            try {
                recorder.start(getOutputMediaFile(session, recordingType, recorder.getName()));
                mSession.sequenceIncrement();
            } catch (Recorder.RecorderException e) {
                Log.e(TAG, "Could not start a recorder: " + recorder);
            }
        }
    }

    public void start(Session session,
                      MediaType recordingType,
                      String[] cameraIDs) throws SimulcorderException {
        if (cameraIDs == null) {
            Log.e(TAG, "Could not start recorders: no camera IDs.");
            throw new SimulcorderException("Could not start recorders: no camera IDs.");
        }
        mState = State.RECORDING;
        mSession = session;
        for (AbstractRecorder recorder : mRecorders.values()) {
            if (Arrays.binarySearch(cameraIDs, recorder.getCameraId()) < 0) {
                continue;
            }
            try {
                recorder.start(getOutputMediaFile(session, recordingType, recorder.getName()));
                mSession.sequenceIncrement();
            } catch (Recorder.RecorderException e) {
                Log.e(TAG, "Could not start a recorder: " + recorder);
                throw new SimulcorderException(e.getMessage());
            }
        }
    }

    /**
     * Stop the recording.
     * @throws Recorder.RecorderException
     */
    public void stop() {
        if (mState == State.STOPPED) {
            return;
        }
        mState = State.STOPPED;
        for (AbstractRecorder recorder : mRecorders.values()) {
            try {
                if (recorder.isRecording()) {
                    mRecordings++;
                }
                recorder.stop();
            } catch (Recorder.RecorderException e) {
                Log.e(TAG, "Could not stop a recorder: " + recorder);
            }
        }
    }

    public int getRecordings() {
        return mRecordings;
    }

    /**
     * Close all the recorders.
     * @throws Recorder.RecorderException
     */
    public void close() {
        mState = State.CLOSED;
        for (AbstractRecorder recorder : mRecorders.values()) {
            try {
                recorder.close();
            } catch (Recorder.RecorderException e) {
                Log.e(TAG, "Could not close a recorder: " + recorder);
            }
        }
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile(Session session, MediaType type, String suffix) {
        if ((type != MediaType.IMAGE) && (type != MediaType.VIDEO)) {
            // TODO: Handle errors properly.
            return null;
        }

        try {
            createDirectories(mMediaStorageDir);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        File mediaFile;
        String namePrefix = "VID";
        String extension = "mp4";

        if (type == MediaType.IMAGE) {
            namePrefix = "IMG";
            extension  = ".jpg";
        }
        String deviceID = session.getDeviceID();
        String timestamp = session.getTimestamp().toString();
        String sequenceNumber = Integer.toString(session.getSequenceNumber());

        return new File(mMediaStorageDir.getPath() + File.separator
                + namePrefix + "_" + deviceID + "_" + timestamp + "_" + sequenceNumber + "_"
                + suffix + "." + extension);
    }

    /**
     * Create full directory path if it does not exist.
     *
     * @param directory -- A full path to a directory to create.
     * @throws Exception on errors.
     */
    private static void createDirectories(File directory) throws Exception {
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                throw new Exception("Failed to create a directory:" +
                        directory.getPath());
            }
        }
    }
}
