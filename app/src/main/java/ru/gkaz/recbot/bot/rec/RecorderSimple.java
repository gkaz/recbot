package ru.gkaz.recbot.bot.rec;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;

import java.io.File;
import java.io.IOException;

/**
 * This recorder uses default MediaRecorder to record video.
 */
public class RecorderSimple extends AbstractRecorder {
    private Camera mCamera;
    private MediaRecorder mMediaRecorder;
    public RecorderSimple(RecorderConfig config) throws RecorderException {
        super(config);
    }

    @Override
    public boolean isRecording() {
        return super.isRecording();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void start(File output) throws RecorderException {
        super.start(output);
        if(mCamera == null) {
            mCamera = Camera.open(Integer.parseInt(mConfig.getCameraId()));
            mCamera.unlock();
        }

        if(mMediaRecorder == null)
            mMediaRecorder = new MediaRecorder();

        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);

        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setOutputFormat(8);
        mMediaRecorder.setOutputFile(output.getAbsolutePath());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setVideoSize(mConfig.getVideoWidth(), mConfig.getVideoHeight());

        mMediaRecorder.setVideoEncodingBitRate(cpHigh.videoBitRate);
        mMediaRecorder.setAudioEncodingBitRate(cpHigh.audioBitRate);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setMaxDuration(-1);

        mMediaRecorder.setAudioSamplingRate(48000);
        mMediaRecorder.setAudioEncodingBitRate(128000);

        try {
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mMediaRecorder.start();
    }

    @Override
    public void stop() throws RecorderException {
        super.stop();
        if (mCamera != null) {
            mCamera.lock();
        }
        if (mMediaRecorder != null) {
            mMediaRecorder.stop();
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
    }
}
