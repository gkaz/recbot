package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class describes a "clear" request.  It is sent when the RecBot clears all the files
 * as response on a UI action.
 */
public class RequestRm extends Request {
    public static final String COMMAND  = "rm";
    public static final String NAME_KEY = "name";
    public RequestRm(String deviceID) {
        super(deviceID, COMMAND);
        try {
            JSONObject args = new JSONObject();
            args.put(NAME_KEY, "*");
            setArguments(args);
        } catch (JSONException e) {
            Log.e(RequestRm.class.getSimpleName(), e.getMessage());
        }
    }
}
