package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;

import ru.gkaz.recbot.bot.net.core.Message;

public class ResponseSuccess extends Response {
    public ResponseSuccess(String deviceID) {
        super(deviceID, Message.RESULT_SUCCESS);
    }

    public ResponseSuccess(String deviceID, Object response) {
        this(deviceID);
        try {
            put(Message.RESPONSE, response);
        } catch (JSONException e) {
            Log.e(ResponseSuccess.class.getSimpleName(), e.getMessage());
        }
    }
}
