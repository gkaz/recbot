package ru.gkaz.recbot.bot.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaFormat;
import android.util.Log;

import androidx.preference.PreferenceManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import ru.gkaz.recbot.R;
import ru.gkaz.recbot.bot.net.ResponseConf;

/**
 * This class describes RecBot configuration.  It allows to load and save configuration from the
 * application shared preferences.
 */
public class Config implements Type {
    public static final String KEY = "conf";

    public static final String FRONT_CAMERA_PREVIEW_STATUS_KEY = "camera-front-preview-enabled?";
    public static final String FRONT_CAMERA_ID_KEY = "camera-front-id";
    public static final String FRONT_CAMERA_VIDEO_FORMAT_KEY = "camera-front-video-format";
    public static final String FRONT_CAMERA_RESOLUTION_KEY   = "camera-front-resolution";
    public static final String REAR_CAMERA_PREVIEW_STATUS_KEY = "camera-rear-preview-enabled?";
    public static final String REAR_CAMERA_ID_KEY = "camera-rear-id";
    public static final String REAR_CAMERA_VIDEO_FORMAT_KEY  = "camera-rear-video-format";
    public static final String REAR_CAMERA_RESOLUTION_KEY   = "camera-rear-resolution";
    public static final String DEVICE_ALIAS_KEY = "device-alias";
    public static final String CAMERA_SWITCH_TIMEOUT_KEY = "camera-switch-timeout";
    public static final String SESSION_AUTO_SEND_KEY     = "session-auto-send?";
    public static final String SESSION_AUTO_REMOVE_KEY   = "session-auto-remove?";
    public static final String SERVER_ADDRESS_KEY        = "server-address";
    public static final String SERVER_PORT_KEY           = "server-port";
    public static final String SERVER_RESOURCE_KEY       = "server-resource";
    public static final String SERVER_USE_TLS_KEY        = "server-use-tls?";
    public static final String MOVEMENT_DETECTION_THRESHOLD_KEY = "movement-detection-threshold";

    /**
     * Default WebSocket port number.
     */
    public static final int DEFAULT_PORT         = 8092;

    /**
     * Default rear camera ID.
     */
    public static final int DEFAULT_CAMERA_BACK  = 0;

    /**
     * Default frontal camera ID.
     */
    public static final int DEFAULT_CAMERA_FRONT = 1;

    public static final float DEFAULT_FRAMERATE = 29.97f;

    private Context mContext;
    private SharedPreferences mSharedPref;

    public Config(Context context) {
        mContext = context;
        mSharedPref =  PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    /**
     * Try to load the server URL from the shared preferences, use default values if no preferences
     * are found.
     *
     * @return the WebSocket server URL.
     */
    public String loadServerURL() {
        String serverIP = mSharedPref.getString(mContext.getString(R.string.server_ip_key),
                null);
        if (serverIP == null) {
            return null;
        }
        serverIP = serverIP.trim();
        String serverPort = mSharedPref.getString(mContext.getString(R.string.server_port_key),
                null);
        if (serverPort == null) {
            serverPort = Integer.toString(DEFAULT_PORT);
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putString(mContext.getString(R.string.server_port_key), serverPort);
            editor.commit();
        }
        boolean useTLS = mSharedPref.getBoolean(mContext.getString(R.string.server_use_tls_key),
                true);
        String url = mSharedPref.getString(mContext.getString(R.string.server_resource_key),
                "");
        String protocol = useTLS ? "https" : "http";
        String serverURL = protocol + "://" + serverIP + ":" + serverPort + url;
        return serverURL;
    }

    public String loadServerAddress() {
       return mSharedPref.getString(mContext.getString(R.string.server_ip_key),
                null);
    }

    public Integer loadServerPort() {
        String port = mSharedPref.getString(mContext.getString(R.string.server_port_key),
                null);
        return (port != null) ? Integer.parseInt(port) : null;
    }

    public String loadServerResource() {
        return mSharedPref.getString(mContext.getString(R.string.server_resource_key),
                "");
    }

    public boolean loadServerUseTLS() {
        return mSharedPref.getBoolean(mContext.getString(R.string.server_use_tls_key),
                true);
    }

    public String loadCameraFrontID() {
        String cameraFront =  mSharedPref.getString(mContext.getString(R.string.camera_front_key),
                null);
        if (cameraFront == null) {
            cameraFront = Integer.toString(DEFAULT_CAMERA_FRONT);
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putString(mContext.getString(R.string.camera_front_key), cameraFront);
            editor.commit();
        }
        return cameraFront;
    }

    public String loadCameraRearID() {
        String cameraBack =  mSharedPref.getString(mContext.getString(R.string.camera_back_key),
                null);
        if (cameraBack == null) {
            cameraBack = Integer.toString(DEFAULT_CAMERA_BACK);
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putString(mContext.getString(R.string.camera_back_key), cameraBack);
            editor.commit();
        }

        return cameraBack;
    }

    /**
     * Load camera ID by its name.
     * @param device Device name ("front" or "rear".)
     * @return a camera ID as a string.
     */
    public String loadCameraID(String device) {
        switch (device) {
            case "front":
                return loadCameraFrontID();
            case "rear":
                return loadCameraRearID();
            default:
                return null;
        }
    }

    public String loadCameraDevice(int id) {
        int frontID = Integer.parseInt(loadCameraFrontID());
        int rearID  = Integer.parseInt(loadCameraRearID());
        if (id == frontID) {
            return "front";
        }
        if (id == rearID) {
            return "rear";
        }

        return "unknown";
    }

    public String loadFrontalResolution() {
        return mSharedPref.getString(
                mContext.getString(R.string.camera_front_resolution_key),
                "640x480");
    }

    public void saveFrontalResolution(@NotNull String resolution) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(mContext.getString(R.string.camera_front_resolution_key),
                resolution);
        editor.commit();
    }

    public String loadRearResolution() {
        return mSharedPref.getString(
                mContext.getString(R.string.camera_rear_resolution_key),
                "640x480");
    }

    public void saveRearResolution(@NotNull String resolution) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(mContext.getString(R.string.camera_rear_resolution_key),
                resolution);
        editor.commit();
    }


    public String loadFrontalFormat() {
        return mSharedPref.getString(
                mContext.getString(R.string.camera_front_video_format_key),
                MediaFormat.MIMETYPE_VIDEO_AVC);
    }

    public String loadRearFormat() {
        return mSharedPref.getString(
                mContext.getString(R.string.camera_rear_video_format_key),
                MediaFormat.MIMETYPE_VIDEO_AVC);
    }

    public float loadFrontalFrameRate() {
        return Float.parseFloat(
                mSharedPref.getString(mContext.getString(R.string.camera_front_framerate_key),
                Float.toString(DEFAULT_FRAMERATE)));
    }

    public float loadRearFrameRate() {
        return Float.parseFloat(
                mSharedPref.getString(mContext.getString(R.string.camera_rear_framerate_key),
                Float.toString(DEFAULT_FRAMERATE)));
    }

    public boolean loadFrontalRecorderType() {
        return mSharedPref.getBoolean(
                mContext.getString(R.string.camera_front_use_old_recorder_key),
                false);
    }

    public boolean loadRearRecorderType() {
        return mSharedPref.getBoolean(
                mContext.getString(R.string.camera_rear_use_old_recorder_key),
                false);
    }

    public void saveFrontalCameraPreviewState(boolean isEnabled) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putBoolean(mContext.getString(R.string.camera_front_preview_key), isEnabled);
        editor.commit();
    }

    public void saveRearCameraPreviewState(boolean isEnabled) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putBoolean(mContext.getString(R.string.camera_back_preview_key), isEnabled);
        editor.commit();
    }

    public boolean loadFrontalCameraPreviewState() {
        return mSharedPref.getBoolean(mContext.getString(R.string.camera_front_preview_key),
                false);
    }

    public boolean loadRearCameraPreviewState() {
        return mSharedPref.getBoolean(mContext.getString(R.string.camera_back_preview_key),
                false);
    }

    public int loadCameraSwitchTimeout() {
        return Integer.parseInt(
                mSharedPref.getString(mContext.getString(R.string.camera_switch_timeout_key),
                        "10"));
    }

    public boolean loadAutoSendData() {
        return mSharedPref.getBoolean(mContext.getString(R.string.common_auto_send_data_on_stop_key),
                false);
    }

    public boolean loadAutoRemoveData() {
        return mSharedPref.getBoolean(
                mContext.getString(R.string.common_auto_remove_data_after_sending_key),
                false);
    }

    public String loadDeviceAlias() {
        return mSharedPref.getString(mContext.getString(R.string.device_alias_key), "");
    }

    /**
     * Get device movement detection threshold.  -1 means that the detection is disabled.
     * @return the threshold as a float value.
     */
    public float loadMovementDetectionThreshold() {
        return Float.parseFloat(
                mSharedPref.getString(
                    mContext.getString(R.string.common_movement_detection_threshold_key),
                    "-1"));
    }

    /**
     * Check if networking stack is enabled in the configuration.  The networking is disabled
     * when the server URL is set to "none".
     *
     * @return true if networking is enabled, false otherwise.
     */
    public boolean isNetworkingEnabled() {
        String ip = mSharedPref.getString(mContext.getString(R.string.server_ip_key),
                "none");
        return (! ip.equals("none"));
    }

    /**
     * Load the device configuration from a JSON object.
     * @param object An JSON object that holds the new device configuration.
     */
    public void fromJSON(JSONObject object) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        try {
            for (Iterator<String> it = object.keys(); it.hasNext(); ) {
                String key = it.next();
                switch (key) {
                    case FRONT_CAMERA_PREVIEW_STATUS_KEY:
                        editor.putBoolean(mContext.getString(R.string.camera_front_preview_key),
                                object.getBoolean(key));
                        break;
                    case REAR_CAMERA_PREVIEW_STATUS_KEY:
                        editor.putBoolean(mContext.getString(R.string.camera_back_preview_key),
                                object.getBoolean(key));
                        break;
                    case FRONT_CAMERA_VIDEO_FORMAT_KEY:
                        editor.putString(mContext.getString(R.string.camera_front_video_format_key),
                                object.getString(key));
                        break;
                    case REAR_CAMERA_VIDEO_FORMAT_KEY:
                        editor.putString(mContext.getString(R.string.camera_rear_video_format_key),
                                object.getString(key));
                        break;
                    case FRONT_CAMERA_RESOLUTION_KEY:
                        editor.putString(mContext.getString(R.string.camera_front_resolution_key),
                                object.getString(key));
                        break;
                    case REAR_CAMERA_RESOLUTION_KEY:
                        editor.putString(mContext.getString(R.string.camera_rear_resolution_key),
                                object.getString(key));
                        break;
                    case DEVICE_ALIAS_KEY:
                        editor.putString(mContext.getString(R.string.device_alias_key),
                                object.getString(key));
                        break;
                    case CAMERA_SWITCH_TIMEOUT_KEY:
                        editor.putString(mContext.getString(R.string.camera_switch_timeout_key),
                                Integer.toString(object.getInt(key)));
                        break;
                    case SESSION_AUTO_SEND_KEY:
                        editor.putBoolean(
                                mContext.getString(R.string.common_auto_send_data_on_stop_key),
                                object.getBoolean(key));
                        break;
                    case SESSION_AUTO_REMOVE_KEY:
                        editor.putBoolean(
                                mContext.getString(R.string.common_auto_remove_data_after_sending_key),
                                object.getBoolean(key));
                        break;
                    case MOVEMENT_DETECTION_THRESHOLD_KEY:
                        editor.putString(
                                mContext.getString(R.string.common_movement_detection_threshold_key),
                                Double.toString(object.getDouble(key)));
                        break;
                    default:
                        Log.i(Config.class.getSimpleName(), "Unhandled option: " + key);
                }
            }
        } catch (JSONException e) {

        }
        editor.commit();
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject conf = new JSONObject();
        try {
            conf.put(SERVER_ADDRESS_KEY,              loadServerAddress());
            conf.put(SERVER_PORT_KEY,                 loadServerPort());
            conf.put(SERVER_RESOURCE_KEY,             loadServerResource());
            conf.put(SERVER_USE_TLS_KEY,              loadServerUseTLS());
            conf.put(DEVICE_ALIAS_KEY,                loadDeviceAlias());
            conf.put(CAMERA_SWITCH_TIMEOUT_KEY,       loadCameraSwitchTimeout());
            conf.put(SESSION_AUTO_SEND_KEY,           loadAutoSendData());
            conf.put(SESSION_AUTO_REMOVE_KEY,         loadAutoRemoveData());
            conf.put(FRONT_CAMERA_PREVIEW_STATUS_KEY, loadFrontalCameraPreviewState());
            conf.put(FRONT_CAMERA_ID_KEY,             loadCameraFrontID());
            conf.put(FRONT_CAMERA_VIDEO_FORMAT_KEY,   loadFrontalFormat());
            conf.put(FRONT_CAMERA_RESOLUTION_KEY,     loadFrontalResolution());
            conf.put(REAR_CAMERA_PREVIEW_STATUS_KEY,  loadRearCameraPreviewState());
            conf.put(REAR_CAMERA_ID_KEY,              loadCameraRearID());
            conf.put(REAR_CAMERA_VIDEO_FORMAT_KEY,    loadRearFormat());
            conf.put(REAR_CAMERA_RESOLUTION_KEY,      loadRearResolution());
            conf.put(MOVEMENT_DETECTION_THRESHOLD_KEY, loadMovementDetectionThreshold());
        } catch (JSONException e) {
            Log.e(ResponseConf.class.getSimpleName(), e.getMessage());
        }
        return conf;
    }
}
