package ru.gkaz.recbot.bot.rec;

public class RecorderConfig {
    private String mCameraId;
    private String mName;
    private String mMimeType;
    private int mVideoWidth;
    private int mVideoHeight;
    private float mFramerate;

    public RecorderConfig setCameraId(String id) {
        mCameraId = id;
        return this;
    }

    public RecorderConfig setMimeType(String type) {
        mMimeType = type;
        return this;
    }

    public RecorderConfig setVideoDimensions(int width, int height) {
        mVideoWidth  = width;
        mVideoHeight = height;
        return this;
    }

    public RecorderConfig setName(String name) {
        mName = name;
        return this;
    }

    public RecorderConfig setFrameRate(float value) {
        mFramerate = value;
        return this;
    }

    public String getCameraId() {
        return mCameraId;
    }

    public String getName() {
        return mName;
    }

    public String getMimeType() {
        return mMimeType;
    }

    public int getVideoWidth() {
        return mVideoWidth;
    }

    public int getVideoHeight() {
        return mVideoHeight;
    }

    public float getFrameRate() {
        return mFramerate;
    }
}
