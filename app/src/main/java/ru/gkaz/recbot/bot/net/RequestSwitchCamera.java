package ru.gkaz.recbot.bot.net;

import org.json.JSONException;

import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.core.Status;

public class RequestSwitchCamera extends Request {
    public static final String COMMAND = "switch-camera";

    public RequestSwitchCamera(Session session, Status status) {
        super(session.getDeviceID(), COMMAND);
        try {
            put(Session.KEY, session.toJSON());
            put(Status.KEY,  status.toJSON());
        } catch (JSONException e) {
            // TODO:
        }
    }

}
