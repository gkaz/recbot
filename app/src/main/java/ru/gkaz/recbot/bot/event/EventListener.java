package ru.gkaz.recbot.bot.event;

/**
 * Interface that describes API to an event listener.
 */
public interface EventListener {
    boolean onStart();
    boolean onStop();
    boolean isStarted();
}
