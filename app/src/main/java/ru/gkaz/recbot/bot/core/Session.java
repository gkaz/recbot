package ru.gkaz.recbot.bot.core;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class describes a recording session.
 */
public class Session implements Type {
    public static final String KEY          = "session";
    public static final String DEVICE_ID    = "device-id";
    public static final String DEVICE_ALIAS = "device-alias";
    public static final String TIMESTAMP    = "timestamp";

    private String mDeviceID;
    private String mDeviceAlias;
    private Long mTimestamp;

    /**
     * Current video sequence number.
     */
    private int mSequenceNumber = 0;

    public Session(String deviceID, String deviceAlias, Long timestamp) {
        mDeviceID    = deviceID;
        mDeviceAlias = deviceAlias;
        mTimestamp   = timestamp;
    }

    /**
     * Get ID of the device.
     * @return Device ID as a String.
     */
    public String getDeviceID() {
        return mDeviceID;
    }

    /**
     * Get the alias (name) of the device.
     * @return Device alias as a String.
     */
    public String getDeviceAlias() {
        return mDeviceAlias;
    }

    /**
     * Get the session timestamp.
     * @return Unix timestamp.
     */
    public Long getTimestamp() {
        return mTimestamp;
    }

    /**
     * Get the current video number in the session.
     * @return Video sequence number.
     */
    public int getSequenceNumber() {
        return mSequenceNumber;
    }

    /**
     * Increment the video sequence number.
     */
    public void sequenceIncrement() {
        mSequenceNumber++;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    /**
     * Convert the session to a JSON object.
     * @return JSON object.
     */
    @Override
    public JSONObject toJSON() {
        JSONObject session = new JSONObject();
        try {
            session.put(DEVICE_ID, mDeviceID);
            session.put(TIMESTAMP, mTimestamp);
            session.put(DEVICE_ALIAS, mDeviceAlias);
        } catch (JSONException e) {
            Log.e(Session.class.getSimpleName(), e.getMessage());
            return null;
        }
        return session;
    }
}
