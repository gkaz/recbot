package ru.gkaz.recbot.bot.net;

import org.json.JSONException;
import org.json.JSONObject;

import ru.gkaz.recbot.bot.net.core.Message;

public class MessageJSON implements Message {
    /**
     * This object contains the message content.
     */
    protected JSONObject mObject;

    public MessageJSON() {
        this(new JSONObject());
    }
    public MessageJSON(JSONObject object) {
        mObject = object;
    }
    public MessageJSON(String jsonString) throws JSONException {
        this(new JSONObject(jsonString));
    }
    /**
     * Put a new data to the message content.
     * @param name Object name.
     * @param object Object value.
     * @return this instance of the MessageJSON to allow 'put' calls to be chained together.
     * @throws JSONException on JSON errors.
     */
    public MessageJSON put(String name, Object object) throws JSONException {
        mObject.put(name, object);
        return this;
    }

    public Object get(String name) throws JSONException {
        return mObject.get(name);
    }

    /**
     * Convert the message content to the binary form.
     * @return an array of bytes.
     */
    public byte[] getContent() {
        return mObject.toString().getBytes();
    }

    /**
     * Convert the message content to a JSON string.
     * @return a JSON string.
     */
    public String toString() {
        return mObject.toString();
    }
}
