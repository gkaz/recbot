package ru.gkaz.recbot.bot.net;

import ru.gkaz.recbot.bot.core.Session;
import ru.gkaz.recbot.bot.core.Status;

public class RequestSessionStop extends RequestSession {
    public RequestSessionStop(Session session, Status status) {
       super(RequestSession.ACTION_STOP, session, status);
    }
}
