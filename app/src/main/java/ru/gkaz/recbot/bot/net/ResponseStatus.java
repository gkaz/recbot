package ru.gkaz.recbot.bot.net;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import ru.gkaz.recbot.bot.core.Status;

public class ResponseStatus extends ResponseSuccess {
   public ResponseStatus(Status status) {
       super(status.getDeviceID());
       try {
           put(Status.KEY, status.toJSON());
       } catch (JSONException e) {
           Log.e(ResponseStatus.class.getSimpleName(), e.getMessage());
       }
   }
}
