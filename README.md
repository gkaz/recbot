# RecBot

This application turns your phone into a remote-controlled video-recording device.

## License

The project was initially written and being developed by “AZ Company Group” LLC
(https://www.gkaz.ru/)  You can find the list of contributors in AUTHORS file.

RecBot is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version. Please see `COPYING` file for the terms of GNU General Public License.

## API

### Server-Client
Here listed commands that can be sent from a RecBotClient (RBC) implementation
to the server (RecBot).

#### `status`
Requests the device status.

Request example:
```json
{ "device-id": "*", "command": "status", "arguments": { } }
```

Response on success:
```json
{ "result" : "success",
  "status" :
   { "device-id"      : "5465033DA6F3",
     "device-alias"   : "test",
     "battery-level"  : 100,
     "uptime"         : 17157021,
     "recording?"     : false,
     "current-camera" : 1,
     "recordings": 9 } }
```

#### `start`
Starts recording on the selected devices.

When `devices` array contains only one device, the recording will be started only for the 
specified device, even when camera switch timeout is set to value greater than zero.

If camera switch timeout is greater than zero and the `devices` array contains two devices, 
the RecBot will start sequential recording on on all the devices specified in the array.

##### Request example
Start the recording only for the frontal camera, ignoring the camera switch timeout
option value:

```json
{ "device-id" : "*", 
  "command"   : "start", 
  "arguments" : {"devices": [ "front" ] } }
```

Start the recording for "front" and "rear" devices:

```json
{ "device-id" : "*",
  "command"   : "start", 
  "arguments" : {"devices": [ "front", "rear" ] } }
```

##### Response on success
```json
{ "result" : "success" }
```

#### `stop`
Stops all the cameras.

Request example:
```json
{ "device-id" : "*", 
  "command"   : "stop", 
  "arguments" : {} }
```

#### `ls`
Lists the recorded files.

Request example:
```json
{ "device-id" : "*", 
  "command"   : "ls",
  "arguments" : { "long?": true } }
```

#### `get`

Request example:
```json
{ "device-id" : "*",
  "command"   : "get",
  "arguments" : { "name": "VID_20201213_165931CAM_FRONT.mp4" } }
```

RecBot will send binary video data as the response.

#### `rm`
Remove all recorded files or a selected file.

Request example to remove just one file:
```json
{ "device-id" : "*", 
  "command"   : "rm",
  "arguments" : { "name": "VID_20201213_165931CAM_FRONT.mp4" } }
```

Request example to remove all recorded files:
```json
{ "device-id" : "*",
  "command"   : "rm",
  "arguments" : { "name": "*" } }
```

#### `conf`
Configure RecBot or get the current configuration and state.

Request example to get the current configuration: 
```json
{ "device-id" : "*",
  "command"   : "conf",
  "arguments" : { } }
```

Request example to enable the preview for one camera:
```json
{ "device-id" : "*",
  "command"   : "conf",
  "arguments" : {
      "conf" : {
          "server-address"                : "172.16.1.1",
          "server-port"                   : 3000,
          "server-resource"               : "\/ws",
          "server-use-tls?"               : false,
          "device-alias"                  : "qwerty",
          "camera-switch-timeout"         : 15,
          "session-auto-send?"            : true,
          "session-auto-remove?"          : false,
          "camera-front-preview-enabled?" : false,
          "camera-front-id"               : "0",
          "camera-front-video-format"     : "video\/avc",
          "camera-front-resolution"       : "640x480",
          "camera-rear-preview-enabled?"  : false,
          "camera-rear-id"                : "1",
          "camera-rear-video-format"      : "video\/avc",
          "camera-rear-resolution"        : "640x480",
          "movement-detection-threshold"  : 1.0
      }
  }
}
```

#### `help`
Get the manual page.

Request example:
```json
{ "device-id" : "*",
  "command"   : "conf",
  "arguments" : { } }
```

Response:
```json
{ "result"   : "success",
  "response" : "Available commands: rec, stop, ls, get, rm, conf, help" }
```

Request the manual page for `get` command:
```json
{ "command"   : "help", 
  "arguments" : { "command" : "get" } }
```

### Client-Server
These commands can be sent by RecBot to the server.

When the device connects to a server, it sends a registration request:
```json
{ "device-id" : "5465033DA6F3", 
  "command"   : "register",
  "status" :
   { "device-id"      : "5465033DA6F3",
     "device-alias"   : "test",
     "battery-level"  : 100,
     "uptime"         : 17157021,
     "recording?"     : false,
     "current-camera" : 1,
     "recordings": 9 } }
```

#### `ping`
This command can be used to test whether the RBC is connected and is capable of handling commands
or not.

##### Request example:
```json
{ "device-id" : "5465033DA6F3",
  "command"   : "ping",
  "arguments" : { } }
``` 

#### `register`
Register the device on the server.

##### Request example
```json
{ "device-id"    : "5465033DA6F3",
  "command"      : "register",
  "device-alias" : "test" }
```

#### `rm`
The device was cleared from the recorded files.

##### Request example

```json
{ "device-id" : "5465033DA6F3",
  "command"   : "rm",
  "arguments" : { "name" : "*" } }
```

#### `session-send`
The device is going to send the recorded data as a sequence of binary chunks.

##### Request example

```json
{ "device-id" : "5465033DA6F3",
  "command"   : "session-send",
  "session"   : {
     "device-id"    : "5465033DA6F3",
     "timestamp"    : 1611603053298,
     "device-alias" : "test"
   },
  "recordings": 1 }
```

#### `session-start`
New recording session is started.

##### Request example

```json
{ "device-id" : "5465033DA6F3",
  "command"   : "session-start",
  "session"   : { 
     "device-id"    : "5465033DA6F3",
     "timestamp"    : 1611602959549,
     "device-alias" : "test" 
  }
}
```

#### `session-stop`
The current recording session is stopped.

#### `switch-camera`
The current recording camera was changed during the recording session.

##### Request example

```json
 { "device-id"     : "5465033DA6F3",
  "command"        : "switch-camera",
  "current-camera" : 1,
  "session"        : { 
     "device-id": "5465033DA6F3",
     "timestamp": 1611602959549,
     "device-alias": "test"
  }
}
```